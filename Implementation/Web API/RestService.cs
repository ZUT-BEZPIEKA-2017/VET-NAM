﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.Net;
using System.IO;
using System.Diagnostics;
using RestSharp;
using RestSharp.Authenticators;
using Newtonsoft.Json;

namespace RestApi
{
    public sealed class RestService
    {
        private static RestService c_RestServiceInstance = null;
        private static readonly object m_Lock = new object();

        public string Login {private get;  set;}
        public string Password { private get; set; }
        private string Token { get; set; }

        public bool IsToken { get;  private set; }
        public bool LastOperationSuccess { get; private set; }

        public static RestService Instance
        {
            get
            {
                lock (m_Lock)
                {
                    if (c_RestServiceInstance == null)
                    {
                        c_RestServiceInstance = new RestService();
                        RestService.Instance.IsToken = false;
                    }
                    return c_RestServiceInstance;
                }
            }
        }
        public static bool GetToken()
        {
            // Kod Grzecha
            try
            {
                HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(Konstanty.Address_Token);
                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/json";

                using (var streamWriter = new StreamWriter(httpRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(new
                    {
                        email = RestService.Instance.Login,
                        password = RestService.Instance.Password,
                    });
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    dynamic ResultToJSON = JsonConvert.DeserializeObject(result);

                    RestService.Instance.Token = ResultToJSON.token;
                    if(RestService.Instance.Token.Length == 0)
                    {
                        RestService.Instance.IsToken = false;
                        RestService.Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
            }
            catch( Exception xXx)
            {
                // jak nie pyknie polaczenie to tu walnie!
                RestService.Instance.IsToken = false;
                RestService.Instance.LastOperationSuccess = false;
                return false;
            }
            // Koniec kodu Grzecha
            RestService.Instance.IsToken = true;
            RestService.Instance.LastOperationSuccess = true;
            return true;
        }
        public static string GetInfo(string Mode)
        {
            if (RestService.Instance.IsToken)
            {
                var Client = new RestClient();
                Client.BaseUrl = new Uri(Konstanty.Address);
                Client.Authenticator = new HttpBasicAuthenticator(RestService.Instance.Login, RestService.Instance.Password);

                string FormattedMode = "api/" + Mode + "/";

                var Request = new RestRequest(FormattedMode, Method.GET);
                Request.RequestFormat = RestSharp.DataFormat.Json;
                Request.AddHeader("Authorization", "Bearer " + RestService.Instance.Token);
                Request.AddHeader("Accept", "application/json");

                IRestResponse Response = Client.Execute(Request);

                RestService.Instance.LastOperationSuccess = true;
                return Response.Content;
            }
            else
            {
                RestService.Instance.LastOperationSuccess = false;
                return "Brak tokenu!";
            }
        }
        public static bool PostInfo(string Mode, string Data)
        {
            if (RestService.Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(RestService.Instance.Login, RestService.Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode, Method.POST);
                    Request.RequestFormat = RestSharp.DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + RestService.Instance.Token);
                    Request.AddHeader("Content-Type", "application/json");

                    object JSON = JsonConvert.DeserializeObject(Data);
                    //Request.AddBody(JSON);

                    Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                    IRestResponse Response = Client.Execute(Request);

                    if(Response.StatusCode == HttpStatusCode.OK)
                    {
                        RestService.Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        RestService.Instance.LastOperationSuccess = false;
                        return false;
                    }
 
                }
                catch
                {
                    RestService.Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                RestService.Instance.LastOperationSuccess = false;
                return false;
            }
        }
        public static bool DelInfo(string Mode, string Data)
        {
            if (RestService.Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(RestService.Instance.Login, RestService.Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode + Data, Method.DELETE);
                    Request.RequestFormat = RestSharp.DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + RestService.Instance.Token);


                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        RestService.Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        RestService.Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    RestService.Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                RestService.Instance.LastOperationSuccess = false;
                return false;
            }
        }
        public static bool PutInfo(string Mode, string Data, string ID)
        {
            if (RestService.Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(RestService.Instance.Login, RestService.Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode + ID, Method.PUT);
                    Request.RequestFormat = RestSharp.DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + RestService.Instance.Token);
                    Request.AddHeader("Content-Type", "application/json");

                    object JSON = JsonConvert.DeserializeObject(Data);
                    //Request.AddBody(JSON);

                    Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        RestService.Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        RestService.Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    RestService.Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                RestService.Instance.LastOperationSuccess = false;
                return false;
            }
        }
    }
}

