﻿namespace LogParser
{
    public class Log
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string SIp { get; set; }
        public string CsMethod { get; set; }
        public string CsUriStem { get; set; }
        public string CsUriQuery { get; set; }
        public string SPort { get; set; }
        public string CsUserName { get; set; }
        public string CIp { get; set; }
        public string CsUserAgent { get; set; }
        public string CsRefferer { get; set; }
        public string ScStatus { get; set; }
        public string ScSubStatus { get; set; }
        public string ScWin32Status { get; set; }
        public string TimeTaken { get; set; }
    }
}