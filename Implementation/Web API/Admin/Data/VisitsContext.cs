﻿using Admin.Models;
using Microsoft.EntityFrameworkCore;

namespace Admin.Data
{
    public class VisitsContext : DbContext
    {
        public DbSet<Visit> Visits { get; set; }

        public VisitsContext(DbContextOptions<VisitsContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBulder)
        {
            modelBulder.Entity<Visit>().ToTable("Visits");
        }
    }
}
