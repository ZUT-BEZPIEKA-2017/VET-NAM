﻿using Admin.Models;
using Microsoft.EntityFrameworkCore;

namespace Admin.Data
{
    public class PetsContext : DbContext
    {
        public DbSet<Pet> Pets { get; set; }

        public PetsContext(DbContextOptions<PetsContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBulder)
        {
            modelBulder.Entity<Pet>().ToTable("Pets");
        }
    }
}
