﻿using Admin.Models;
using Microsoft.EntityFrameworkCore;

namespace Admin.Data
{
    public class PaymentsContext : DbContext
    {
        public DbSet<Payment> Payments { get; set; }

        public PaymentsContext(DbContextOptions<PaymentsContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBulder)
        {
            modelBulder.Entity<Payment>().ToTable("Payments");
        }
    }
}
