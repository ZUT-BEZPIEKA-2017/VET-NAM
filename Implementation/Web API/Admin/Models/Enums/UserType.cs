﻿namespace Admin.Models.Enums
{
    public enum UserType
    {
        Client,
        Vet,
        Admin
    }
}
