﻿namespace Admin.Models.Enums
{
    public enum PaymentStatus
    {
        Completed,
        InProgress
    }
}
