﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Admin.Models.Enums;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Admin.Models
{
    public class ApplicationUser : IdentityUser
    {
        [JsonProperty("name")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string Name { get; set; }

        [JsonProperty("surname")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string Surname { get; set; }

        [JsonProperty("sex")]
        [EnumDataType(typeof(Sex))]
        [JsonConverter(typeof(StringEnumConverter))]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public Sex Sex { get; set; }

        [JsonProperty("registerDate")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public DateTime RegisterDate { get; set; }

        [JsonProperty("userType")]
        [EnumDataType(typeof(UserType))]
        [JsonConverter(typeof(StringEnumConverter))]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public UserType Type { get; set; }

        [JsonProperty("lastCorrectLogin")]
        public DateTime LastCorrectLogin { get; set; }

        [JsonProperty("nowCorrectLogin")]
        public DateTime NowCorrectLogin { get; set; }

        [JsonProperty("lastPasswordChange")]
        public DateTime LastPasswordChange { get; set; }
    }
}
