﻿using System;
using System.ComponentModel.DataAnnotations;
using Admin.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Admin.Models
{
    public class Payment
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("amount")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public float? Amount { get; set; }

        [JsonProperty("pymentDate")]
        public DateTime? PaymentDate { get; set; }

        [JsonProperty("status")]
        [EnumDataType(typeof(PaymentStatus))]
        [JsonConverter(typeof(StringEnumConverter))]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public PaymentStatus? Status { get; set; }

        [JsonProperty("user")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string User { get; set; }
    }
}
