﻿using System.ComponentModel.DataAnnotations;
using Admin.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Admin.Models
{
    public class Pet
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string Name { get; set; }

        [JsonProperty("owner")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string Owner { get; set; }

        [JsonProperty("breed")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string Breed { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("sex")]
        [EnumDataType(typeof(Sex))]
        [JsonConverter(typeof(StringEnumConverter))]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public Sex? Sex { get; set; }

        [JsonProperty("vaccinated")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public bool? Vaccinated { get; set; }

        [JsonProperty("address")]
        [Required(ErrorMessageResourceType = typeof(Resources.Errors.ModelDataValidation), ErrorMessageResourceName = "EmptyField")]
        public string Address { get; set; }
    }
}
