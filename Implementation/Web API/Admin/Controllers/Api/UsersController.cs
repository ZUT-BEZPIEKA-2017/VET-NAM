using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Admin.Configuration;
using Admin.Data;
using Admin.Models;
using Admin.Models.Enums;
using Admin.ViewModels.AccountViewModels;
using Admin.ViewModels.ManageViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Admin.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/users")]
    public class UsersController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly PasswordHasher<ApplicationUser> _passwordHasher;
        private readonly JwtConfig _jwtConfig;
        private readonly PetsContext _petsContext;
        private readonly PaymentsContext _paymentsContext;
        private readonly VisitsContext _visitsContext;
        private readonly ILogger _logger;

        public UsersController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<JwtConfig> jwtConfig, PetsContext petsContext, ILoggerFactory loggerFactory, PaymentsContext paymentsContext, VisitsContext visitsContext)
        {
            _userManager = userManager;
            _petsContext = petsContext;
            _paymentsContext = paymentsContext;
            _visitsContext = visitsContext;
            _passwordHasher = new PasswordHasher<ApplicationUser>();
            _jwtConfig = jwtConfig.Value;
            _logger = loggerFactory.CreateLogger<UsersController>();
        }

        [HttpGet]
        [SwaggerResponse(200, typeof(ApplicationUser))]
        public JsonResult GetAllUsers()
        {
            _logger.LogInformation("Getting all users.");
            return Json(_userManager.Users);
        }

        [HttpGet("{id}")]
        [SwaggerResponse(200, typeof(ApplicationUser))]
        public JsonResult GetUser(string id)
        {
            _logger.LogInformation($"Getting specified user{id}");
            return Json(_userManager.Users.Where(x => string.Equals(x.Id, id)));
        }

        [HttpGet("vets")]
        [SwaggerResponse(200, typeof(ApplicationUser))]
        public JsonResult GetVets()
        {
            _logger.LogInformation("Getting all vets.");
            return Json(_userManager.Users.Where(x => x.Type == UserType.Vet));
        }

        [HttpDelete("delete/{id}/{deletePetsFlag}")]
        public async Task<IActionResult> DeleteUser(string id, bool deletePetsFlag = false)
        {
            _logger.LogInformation($"Deleting user:{id}; deletePetsFlag = {deletePetsFlag}");
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                _logger.LogError($"User not found:{id}");
                return BadRequest(Json("User not found"));
            }
            if (user.Type == UserType.Admin)
            {
                _logger.LogError($"You cannot delete this user:{id}");
                return BadRequest(Json("You cannot delete this user"));
            }
            var pets = FindOwnersPetsById(id);
            foreach (var pet in pets)
            {
                if (deletePetsFlag)
                {
                    var result = _petsContext.Remove(pet);
                    if (result.State == EntityState.Deleted)
                    {
                        await _petsContext.SaveChangesAsync();
                        _logger.LogInformation($"Pet deleted: {Json(pet)}");
                    }
                }
                else
                {
                    pet.Owner = null;
                    var result = _petsContext.Update(pet);
                    if (result.State == EntityState.Modified)
                    {
                        await _petsContext.SaveChangesAsync();
                        _logger.LogInformation($"Pet's owner set to null. Pet:{result.Entity.Id}");
                    }
                }
            }

            Payment p = FindPaymentByUserId(user.Id);
            if (p != null)
            {
                p.User = null;
                var resultP = _paymentsContext.Update(p);
                if (resultP.State == EntityState.Modified)
                {
                    await _paymentsContext.SaveChangesAsync();
                    _logger.LogInformation($"Payment has been updated. {Json(p)}");
                }
            }

            if (user.Type == UserType.Client)
            {
                Visit v = FindVisitByUserId(user.Id);
                if (v != null)
                {
                    v.User = null;
                    var resultV = _visitsContext.Update(v);
                    if (resultV.State == EntityState.Modified)
                    {
                        await _visitsContext.SaveChangesAsync();
                        _logger.LogInformation($"Visit has been updated. {Json(v)}");
                    }
                }
            }
            else if (user.Type == UserType.Vet)
            {
                Visit v = FindVisitByVetId(user.Id);
                if (v != null)
                {
                    v.Vet = null;
                    var resultV = _visitsContext.Update(v);
                    if (resultV.State == EntityState.Modified)
                    {
                        await _visitsContext.SaveChangesAsync();
                        _logger.LogInformation($"Visit has been updated. {Json(v)}");
                    }
                }
            }

            await _userManager.DeleteAsync(user);
            _logger.LogInformation($"User deleted. {Json(user.Email)}");
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Create([FromBody] RegisterViewModel model)
        {
            _logger.LogInformation($"User register in progress: {model.Email}");
            if (!ModelState.IsValid)
            {
                _logger.LogError("Model is not valid.");
                return BadRequest(ModelState.Values.SelectMany(v => v.Errors).Select(modelError => modelError.ErrorMessage).ToList());
            }
            if (model.Type == UserType.Admin && _userManager.Users.Any(x => x.Type == UserType.Admin))
            {
                _logger.LogError("There can be only one account with administrator privileges.");
                return BadRequest(Json("There can be only one account with administrator privileges"));
            }

            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                Name = model.Name,
                Surname = model.Surname,
                Sex = model.Sex,
                PhoneNumber = model.PhoneNumber,
                RegisterDate = DateTime.UtcNow,
                Type = model.Type,
                LockoutEnabled = false,
                LastCorrectLogin = DateTime.UtcNow,
                NowCorrectLogin = DateTime.UtcNow,
                LastPasswordChange = DateTime.UtcNow
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                _logger.LogError($"User registration failed: {user.Email}");
                return BadRequest(result.Errors.Select(x => x.Description).ToList());
            }
            
            _logger.LogInformation($"Created new user: {user.Email}");
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("token")]
        public async Task<IActionResult> Token([FromBody] LoginViewModel model)
        {
            _logger.LogInformation($"Getting new token for user: {model.Email}");
            if (!ModelState.IsValid)
            {
                _logger.LogError("Model is not valid.");
                return BadRequest();
            }

            var user = await _userManager.FindByNameAsync(model.Email);

            if (user == null)
            {
                return BadRequest(Json("Incorrect credentials."));
            }
            if (!user.LockoutEnabled && user.AccessFailedCount >= 3)
            {
                user.LockoutEnabled = true;
                await _userManager.UpdateAsync(user);
            }
            if (user.LockoutEnabled)
            {
                return BadRequest(Json("User has been blocked due to many failed login attempts. Contact the administrator to unlock the account."));
            }
            if (_passwordHasher.VerifyHashedPassword(user, user.PasswordHash, model.Password) != PasswordVerificationResult.Success)
            {
                _logger.LogError("Incorrect credentials.");
                await _userManager.AccessFailedAsync(user);
                return BadRequest(Json("Incorrect credentials."));
            }
            if (DateTime.UtcNow > user.LastPasswordChange.AddDays(30))
            {
                return BadRequest(Json("Your password has expired. Change it first."));
            }

            var token = await GetJwtSecurityToken(user);
            _logger.LogInformation("Token has been generated.");
            user.LastCorrectLogin = user.NowCorrectLogin;
            user.NowCorrectLogin = DateTime.UtcNow;
            user.AccessFailedCount = 0;
            await _userManager.UpdateAsync(user);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo,
                userId = user.Id,
                userType = user.Type
            });
        }

        [HttpPost("changePassword/{id}")]
        public async Task<IActionResult> ChangePassword(string id, [FromBody] ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogError("Passwords do not match.");
                return BadRequest();
            }
            if (string.Equals(model.OldPassword, model.NewPassword))
            {
                return BadRequest(Json("New password cannot be the same as the old one."));
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                try
                {
                    var validateOldPass = await _userManager.CheckPasswordAsync(user, model.OldPassword);
                    if (!validateOldPass)
                    {
                        return BadRequest(Json("Old password do not match to this user."));
                    }
                    var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation($"User({id}) changed password.");
                        user.LastPasswordChange = DateTime.UtcNow;
                        await _userManager.UpdateAsync(user);
                        return Ok();
                    }
                    return BadRequest(Json($"Change password failed. {result.Errors.Select(x => x.Description).Aggregate((s, s1) => s + ' ' + s1)}"));
                }
                catch (Exception ex)
                {
                    return BadRequest(Json(ex.Message));
                }

            }
            return BadRequest(Json("User not found."));
        }

        [HttpPut("unlock/{id}")]
        public async Task<IActionResult> Unlock(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return BadRequest(Json("User not found."));
            }
            user.AccessFailedCount = 0;
            user.LockoutEnabled = false;
            await _userManager.UpdateAsync(user);
            return Ok();
        }

        private async Task<JwtSecurityToken> GetJwtSecurityToken(ApplicationUser user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);

            return new JwtSecurityToken(
                issuer: _jwtConfig.ValidIssuer,
                audience: _jwtConfig.ValidAudience,
                claims: GetTokenClaims(user).Union(userClaims),
                expires: DateTime.UtcNow.AddMinutes(_jwtConfig.ExpirationTime),
                signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtConfig.SecretKey)), SecurityAlgorithms.HmacSha256)
            );
        }

        private static IEnumerable<Claim> GetTokenClaims(ApplicationUser user)
        {
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName)
            };
        }

        private List<Pet> FindOwnersPetsById(string ownerId)
        {
            List<Pet> pets = new List<Pet>();
            foreach (var p in _petsContext.Pets)
            {
                if (p.Owner == ownerId)
                {
                    pets.Add(p);
                    break;
                }
            }
            return pets;
        }

        private Payment FindPaymentByUserId(string userId)
        {
            Payment payment = null;
            foreach (var p in _paymentsContext.Payments)
            {
                if (p.User == userId)
                {
                    payment = p;
                    break;
                }
            }
            return payment;
        }

        private Visit FindVisitByUserId(string userId)
        {
            Visit visit = null;
            foreach (var v in _visitsContext.Visits)
            {
                if (v.User == userId)
                {
                    visit = v;
                    break;
                }
            }
            return visit;
        }

        private Visit FindVisitByVetId(string vetId)
        {
            Visit visit = null;
            foreach (var v in _visitsContext.Visits)
            {
                if (v.Vet == vetId)
                {
                    visit = v;
                    break;
                }
            }
            return visit;
        }
    }
}
