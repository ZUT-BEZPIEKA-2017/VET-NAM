﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Admin.Data;
using Admin.Models;
using Admin.Models.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Admin.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/visits")]
    public class VisitsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly PetsContext _petsContext;
        private readonly VisitsContext _visitsContext;
        private readonly PaymentsContext _paymentsContext;
        private readonly ILogger _logger;

        public VisitsController(VisitsContext visitsContext, UserManager<ApplicationUser> userManager, PetsContext petsContext, PaymentsContext paymentsContext, ILoggerFactory loggerFactory)
        {
            _visitsContext = visitsContext;
            _userManager = userManager;
            _petsContext = petsContext;
            _paymentsContext = paymentsContext;
            _logger = loggerFactory.CreateLogger<VisitsController>();
        }

        [HttpGet]
        [SwaggerResponse(200, typeof(Visit))]
        public JsonResult GetAllVisits()
        {
            _logger.LogInformation($"Getting all visits.");
            return Json(_visitsContext.Visits);
        }

        [HttpGet("{id}")]
        [SwaggerResponse(200, typeof(Visit))]
        public JsonResult GetVisit(string id)
        {
            _logger.LogInformation($"Getting specified visit:{id}");
            return Json(_visitsContext.Visits.Where(x => string.Equals(x.Id, id)));
        }

        [HttpGet("user/{id}")]
        [SwaggerResponse(200, typeof(Visit))]
        public JsonResult GetUserVisits(string id)
        {
            _logger.LogInformation($"Getting user's visits. User:{id}");
            return Json(_visitsContext.Visits.Where(x => string.Equals(x.User, id)));
        }

        [HttpGet("pet/{id}")]
        [SwaggerResponse(200, typeof(Visit))]
        public JsonResult GetPetVisits(string id)
        {
            _logger.LogInformation($"Getting pet's visits. Pet:{Json(id)}");
            return Json(_visitsContext.Visits.Where(x => string.Equals(x.Pet, id)));
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddVisit([FromBody] Visit visit)
        {
            _logger.LogInformation($"Adding visit. {Json(visit)}");
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Model is not valid. {Json(visit)}");
                return BadRequest(ModelState.Values.SelectMany(v => v.Errors).Select(modelError => modelError.ErrorMessage).ToList());
            }

            var pet = FindPetById(visit.Pet);
            if (pet == null)
            {
                _logger.LogError($"Pet not found:{visit.Pet}");
                return BadRequest(Json("Pet not found!"));
            }

            var vet = _userManager.FindByIdAsync(visit.Vet);
            if (vet == null || vet.Result.Type != UserType.Vet)
            {
                _logger.LogError($"Vet not found:{visit.Vet}");
                return BadRequest(Json("Vet not found!"));
            }

            var user = _userManager.FindByIdAsync(visit.User);
            if (user == null || user.Result.Type != UserType.Client)
            {
                _logger.LogError($"User not found:{visit.User}");
                return BadRequest(Json("User not found!"));
            }

            var vis = new Visit
            {
                Id = visit.Id ?? Guid.NewGuid().ToString(),
                Description = visit.Description,
                Duration = visit.Duration,
                StartDate = visit.StartDate ?? DateTime.UtcNow,
                EndDate = (visit.StartDate ?? DateTime.UtcNow).Add(visit.Duration.Value),
                Payment = visit.Payment,
                Pet = visit.Pet,
                User = visit.User,
                Vet = visit.Vet
            };

            try
            {
                var result = await _visitsContext.AddAsync(vis);
                if (result.State == EntityState.Added)
                {
                    await _visitsContext.SaveChangesAsync();
                    _logger.LogInformation($"Visit has been added. {Json(vis)}");
                    return Ok();
                }
            }
            catch (SqlException)
            {
                _logger.LogError($"Incorrect input data. {Json(visit)}");
                return BadRequest(Json("Incorrect input data"));
            }
            catch (DbUpdateException)
            {
                _logger.LogError($"Incorrect input data. {Json(visit)}");
                return BadRequest(Json("Incorrect input data"));
            }
            return BadRequest();
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeleteVisit(string id)
        {
            _logger.LogInformation($"Deleting visit:{id}");
            try
            {
                Visit v = FindVisitById(id);
                if (v == null)
                {
                    _logger.LogError($"Visit not found:{id}");
                    return BadRequest(Json("Visit not found"));
                }

                var p = FindVisitPaymentById(v.Payment);
                if (p != null)
                {
                    var r = _paymentsContext.Remove(p);
                    if (r.State == EntityState.Deleted)
                    {
                        await _paymentsContext.SaveChangesAsync();
                        _logger.LogInformation($"Payment({p.Id}) for visit({id}) has been deleted.");
                    } 
                }

                var result = _visitsContext.Remove(v);
                if (result.State == EntityState.Deleted)
                {
                    await _visitsContext.SaveChangesAsync();
                    _logger.LogInformation($"Visit has been deleted. {Json(v)}");
                    return Ok();
                }
            }
            catch (ArgumentNullException)
            {
                _logger.LogError($"Visit not found:{id}");
                return BadRequest(Json("Visit not found"));
            }
            return BadRequest();
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdateVisit(string id, [FromBody] Visit visit)
        {
            _logger.LogInformation($"Updating visit:{id}");
            try
            {
                Visit v = FindVisitById(id);
                if (v == null)
                {
                    _logger.LogError($"Visit not found:{id}");
                    return BadRequest(Json("Visit not found"));
                }
                var p = FindPaymentById(visit.Payment ?? v.Payment);
                if (p == null)
                {
                    _logger.LogError($"Payment for visit not found. {Json(visit)}");
                    return BadRequest(Json("Payment not found!"));
                }

                v.Pet = visit.Pet ?? v.Pet;
                v.User = visit.User ?? v.User;
                v.Vet = visit.Vet ?? v.Vet;
                v.Description = visit.Description ?? v.Description;
                v.Duration = visit.Duration ?? v.Duration;
                v.StartDate = visit.StartDate ?? v.StartDate;
                v.EndDate = v.StartDate.Value.Add(v.Duration.Value);
                v.Payment = visit.Payment ?? v.Payment;

                var result = _visitsContext.Update(v);
                if (result.State == EntityState.Modified)
                {
                    await _visitsContext.SaveChangesAsync();
                    _logger.LogInformation($"Visit has been updated. {Json(v)}");
                    return Ok();
                }
            }
            catch (DbUpdateException)
            {
                _logger.LogError($"Database update error. {Json(visit)}");
                return BadRequest(Json("Database update error"));
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}");
                return BadRequest(ex.Message);
            }
            return BadRequest();
        }

        private Visit FindVisitById(string id)
        {
            Visit visit = null;
            foreach (var v in _visitsContext.Visits)
            {
                if (v.Id == id)
                {
                    visit = v;
                    break;
                }
            }
            return visit;
        }

        private Pet FindPetById(string id)
        {
            Pet pet = null;
            foreach (var p in _petsContext.Pets)
            {
                if (p.Id == id)
                {
                    pet = p;
                    break;
                }
            }
            return pet;
        }

        private Payment FindVisitPaymentById(string visitPaymentId)
        {
            Payment payment = null;
            foreach (var p in _paymentsContext.Payments)
            {
                if (p.Id == visitPaymentId)
                {
                    payment = p;
                    break;
                }
            }
            return payment;
        }

        private Payment FindPaymentById(string id)
        {
            Payment payment = null;
            foreach (var p in _paymentsContext.Payments)
            {
                if (p.Id == id)
                {
                    payment = p;
                    break;
                }
            }
            return payment;
        }
    }
}
