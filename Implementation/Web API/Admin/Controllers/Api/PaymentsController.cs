﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Admin.Data;
using Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Admin.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/payments")]
    public class PaymentsController : Controller
    {
        private readonly PaymentsContext _paymentsContext;
        private readonly VisitsContext _visitsContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;

        public PaymentsController(PaymentsContext paymentsContext, UserManager<ApplicationUser> userManager, ILoggerFactory loggerFactory, VisitsContext visitsContext)
        {
            _paymentsContext = paymentsContext;
            _userManager = userManager;
            _visitsContext = visitsContext;
            _logger = loggerFactory.CreateLogger<PaymentsController>();
        }

        [HttpGet]
        [SwaggerResponse(200, typeof(Payment))]
        public JsonResult GetAllPayments()
        {
            _logger.LogInformation("Getting all payments.");
            return Json(_paymentsContext.Payments);
        }

        [HttpGet("{id}")]
        [SwaggerResponse(200, typeof(Payment))]
        public JsonResult GetPayment(string id)
        {
            _logger.LogInformation($"Getting specified payment. Payment:{id}");
            return Json(_paymentsContext.Payments.Where(x => string.Equals(x.Id, id)));
        }

        [HttpGet("user/{id}")]
        [SwaggerResponse(200, typeof(Payment))]
        public JsonResult GetUserPayments(string id)
        {
            _logger.LogInformation($"Getting user's payments. User:{id}");
            return Json(_paymentsContext.Payments.Where(x => string.Equals(x.User, id)));
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddPayment([FromBody] Payment payment)
        {
            _logger.LogInformation($"Adding payment. {Json(payment)}");
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Payment model not valid. {Json(payment)}");
                return BadRequest(ModelState.Values.SelectMany(v => v.Errors).Select(modelError => modelError.ErrorMessage).ToList());
            }

            var u = await _userManager.FindByIdAsync(payment.User);
            if (u == null)
            {
                _logger.LogError($"User not found while adding payment:{Json(payment)}");
                return BadRequest(Json("User not found!"));
            }

            var p = new Payment
            {
                Id = payment.Id ?? Guid.NewGuid().ToString(),
                Amount = payment.Amount,
                PaymentDate = payment.PaymentDate ?? DateTime.UtcNow,
                Status = payment.Status,
                User = payment.User
            };

            try
            {
                var result = await _paymentsContext.AddAsync(p);
                if (result.State == EntityState.Added)
                {
                    await _paymentsContext.SaveChangesAsync();
                    _logger.LogInformation($"Payment added. {Json(p)}");
                    return Ok();
                }
            }
            catch (SqlException)
            {
                _logger.LogError($"Incorrect input data. {Json(p)}");
                return BadRequest(Json("Incorrect input data"));
            }
            catch (DbUpdateException)
            {
                _logger.LogError($"Incorrect input data. {Json(p)}");
                return BadRequest(Json("Incorrect input data"));
            }
            return BadRequest();
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletePayment(string id)
        {
            _logger.LogInformation($"Deleting payment:{id}");
            try
            {
                Payment p = FindPaymentById(id);
                if (p == null)
                {
                    _logger.LogError($"Payment not found:{Json(id)}");
                    return BadRequest(Json("Payment not found"));
                }

                Visit v = FindVisitByPaymentId(p.Id);
                if (v != null)
                {
                    v.Payment = null;
                    var resultV = _visitsContext.Update(v);
                    if (resultV.State == EntityState.Modified)
                    {
                        await _visitsContext.SaveChangesAsync();
                        _logger.LogInformation($"Visit has been updated. {Json(v)}");
                    }
                }

                var result = _paymentsContext.Remove(p);
                if (result.State == EntityState.Deleted)
                {
                    await _paymentsContext.SaveChangesAsync();
                    _logger.LogInformation($"Payment deleted. {Json(p)}");
                    return Ok();
                }
            }
            catch (ArgumentNullException)
            {
                _logger.LogError($"Payment not found:{Json(id)}");
                return BadRequest(Json("Payment not found"));
            }
            return BadRequest();
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdatePayment(string id, [FromBody] Payment payment)
        {
            _logger.LogInformation($"Updating payment:{id}");
            try
            {
                Payment p = FindPaymentById(id);
                if (p == null)
                {
                    _logger.LogError($"Payment not found. Payment:{id}");
                    return BadRequest(Json("Payment not found"));
                }

                p.Amount = payment.Amount ?? p.Amount;
                p.PaymentDate = payment.PaymentDate ?? p.PaymentDate;
                p.Status = payment.Status ?? p.Status;
                p.User = payment.User ?? p.User;

                var result = _paymentsContext.Update(p);
                if (result.State == EntityState.Modified)
                {
                    await _paymentsContext.SaveChangesAsync();
                    _logger.LogInformation($"Payment updated. {Json(p)}");
                    return Ok();
                }
            }
            catch (DbUpdateException)
            {
                _logger.LogError($"Database update error. Payment:{id}");
                return BadRequest(Json("Database update error"));
            }
            catch (Exception)
            {
                return BadRequest();
            }
            return BadRequest();
        }

        private Payment FindPaymentById(string id)
        {
            Payment payment = null;
            foreach (var p in _paymentsContext.Payments)
            {
                if (p.Id == id)
                {
                    payment = p;
                    break;
                }
            }
            return payment;
        }

        private Visit FindVisitByPaymentId(string paymentId)
        {
            Visit visit = null;
            foreach (var v in _visitsContext.Visits)
            {
                if (v.Payment == paymentId)
                {
                    visit = v;
                    break;
                }
            }
            return visit;
        }
    }
}
