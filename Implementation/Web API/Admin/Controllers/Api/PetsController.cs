﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Admin.Data;
using Admin.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Admin.Controllers.Api
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/pets")]
    public class PetsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly PetsContext _petsContext;
        private readonly VisitsContext _visitsContext;
        private readonly ILogger _logger;

        public PetsController(PetsContext petsContext, UserManager<ApplicationUser> userManager, ILoggerFactory loggerFactory, VisitsContext visitsContext)
        {
            _petsContext = petsContext;
            _userManager = userManager;
            _visitsContext = visitsContext;
            _logger = loggerFactory.CreateLogger<PetsController>();
        }

        [HttpGet]
        [SwaggerResponse(200, typeof(Pet))]
        public JsonResult GetAllPets()
        {
            _logger.LogInformation("Getting all pets.");
            return Json(_petsContext.Pets);
        }

        [HttpGet("{id}")]
        [SwaggerResponse(200, typeof(Pet))]
        public JsonResult GetPet(string id)
        {
            _logger.LogInformation($"Getting specified pet:{id}");
            return Json(_petsContext.Pets.Where(x => string.Equals(x.Id, id)));
        }

        [HttpGet("user/{id}")]
        [SwaggerResponse(200, typeof(Pet))]
        public JsonResult GetUserPets(string id)
        {
            _logger.LogInformation($"Getting user's pets. User:{id}");
            return Json(_petsContext.Pets.Where(x => string.Equals(x.Owner, id)));
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddPet([FromBody] Pet pet)
        {
            _logger.LogInformation($"Adding pet. {Json(pet)}");
            if (!ModelState.IsValid)
            {
                _logger.LogError($"Pet model is not valid. {Json(pet)}");
                return BadRequest(ModelState.Values.SelectMany(v => v.Errors).Select(modelError => modelError.ErrorMessage).ToList());
            }

            var owner = FindOwnerById(pet.Owner);
            if (owner == null)
            {
                _logger.LogError($"Owner not found. Pet:{Json(pet)}");
                return BadRequest(Json("Owner not found!"));
            }

            var p = new Pet
            {
                Id = pet.Id ?? Guid.NewGuid().ToString(),
                Breed = pet.Breed,
                Description = pet.Description,
                Name = pet.Name,
                Owner = pet.Owner,
                Sex = pet.Sex,
                Address = pet.Address,
                Vaccinated = pet.Vaccinated
            };

            try
            {
                var result = await _petsContext.AddAsync(p);
                if (result.State == EntityState.Added)
                {
                    await _petsContext.SaveChangesAsync();
                    _logger.LogInformation($"Pet added. {Json(p)}");
                    return Ok();
                }
            }
            catch (SqlException)
            {
                _logger.LogError($"Incorrect input data. {Json(p)}");
                return BadRequest(Json("Incorrect input data"));
            }
            catch (DbUpdateException)
            {
                _logger.LogError($"Incorrect input data. {Json(p)}");
                return BadRequest(Json("Incorrect input data"));
            }
            return BadRequest();
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletePet(string id)
        {
            _logger.LogInformation($"Deleting pet:{id}");
            try
            {
                Pet p = FindPetById(id);
                if (p == null)
                {
                    _logger.LogError($"Pet not found:{id}");
                    return BadRequest(Json("Pet not found"));
                }

                Visit v = FindVisitByPetId(p.Id);
                if (v != null)
                {
                    v.Pet = null;
                    var resultV = _visitsContext.Update(v);
                    if (resultV.State == EntityState.Modified)
                    {
                        await _visitsContext.SaveChangesAsync();
                        _logger.LogInformation($"Visit has been updated. {Json(v)}");
                    } 
                }

                var result = _petsContext.Remove(p);
                if (result.State == EntityState.Deleted)
                {
                    await _petsContext.SaveChangesAsync();
                    _logger.LogInformation($"Pet deleted. {Json(p)}");
                    return Ok();
                }
            }
            catch (ArgumentNullException)
            {
                _logger.LogError($"Pet not found:{id}");
                return BadRequest(Json("Pet not found"));
            }
            return BadRequest();
        }

        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdatePet(string id, [FromBody] Pet pet)
        {
            _logger.LogInformation($"Updating pet:{id}");
            try
            {
                Pet p = FindPetById(id);
                if (p == null)
                {
                    _logger.LogError($"Pet not found:{id}");
                    return BadRequest(Json("Pet not found"));
                }
                var owner = FindOwnerById(pet.Owner ?? p.Owner);
                if (owner == null)
                {
                    _logger.LogError($"Owner not found. {Json(p)}");
                    return BadRequest(Json("Owner not found!"));
                }
                
                p.Breed = pet.Breed ?? p.Breed;
                p.Description = pet.Description ?? p.Description;
                p.Name = pet.Name ?? p.Name;
                p.Owner = owner.Id ?? p.Owner;
                p.Sex = pet.Sex ?? p.Sex;
                p.Address = pet.Address ?? p.Address;
                p.Vaccinated = pet.Vaccinated ?? p.Vaccinated;

                var result = _petsContext.Update(p);
                if (result.State == EntityState.Modified)
                {
                    await _petsContext.SaveChangesAsync();
                    _logger.LogInformation($"Pet updated. {Json(p)}");
                    return Ok();
                }
            }
            catch (DbUpdateException)
            {
                _logger.LogError($"Database update error. Pet:{id}");
                return BadRequest(Json("Database update error"));
            }
            catch (Exception)
            {
                return BadRequest();
            }
            return BadRequest();
        }

        private Pet FindPetById(string id)
        {
            Pet pet = null;
            foreach (var p in _petsContext.Pets)
            {
                if (p.Id == id)
                {
                    pet = p;
                    break;
                }
            }
            return pet;
        }

        private ApplicationUser FindOwnerById(string id)
        {
            ApplicationUser owner = null;
            foreach (var o in _userManager.Users)
            {
                if (o.Id == id)
                {
                    owner = o;
                    break;
                }
            }
            return owner;
        }

        private Visit FindVisitByPetId(string petId)
        {
            Visit visit = null;
            foreach (var v in _visitsContext.Visits)
            {
                if (v.Pet == petId)
                {
                    visit = v;
                    break;
                }
            }
            return visit;
        }
    }
}
