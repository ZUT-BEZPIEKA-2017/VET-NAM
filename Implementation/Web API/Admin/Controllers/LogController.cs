﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LogParser;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace Admin.Controllers
{
    [Authorize]
    public class LogController : Controller
    {
        private readonly List<Log> _logs = Parser.DataLogModels;

        public IActionResult Index()
        {
            return View(_logs);
        }

        public async Task<IActionResult> ReadLog(IFormFile logFile)
        {
            try
            {
                if (logFile != null)
                {
                    var filePath = Path.GetTempFileName();
                    if (logFile.Length > 0)
                    {
                        using (var stream = new FileStream(filePath, FileMode.Create))
                        {
                            await logFile.CopyToAsync(stream);
                        }
                    }

                    Parser parser = new Parser(filePath);
                    parser.Parse();
                    Parser.DataLogModels = parser.GetDataModel();
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
