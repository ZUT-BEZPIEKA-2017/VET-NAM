﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using WET_NAM_APP.APIclass;

namespace WET_NAM_APP.MAIN_Login_Register
{
    public partial class MainWindow : Window
    {
        private Window _userMainWindow;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            var regex2 =
            new Regex("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            var match2 = regex2.Match(EmailTextBox.Text);
            if (!match2.Success)
            {
                MessageBox.Show(this, "Podano niepoprawny adres E-mail", "Błąd - Nie można zalogować uźytkownika",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
            } 
            else
            {
                RestService.Instance.Login = EmailTextBox.Text;
                RestService.Instance.Password = PasswordTextBox.Password;

                if (RestService.GetToken())
                {
                    if (RestService.Instance.UserType == "Admin")
                    {
                        _userMainWindow = new AdminWindow.MainWindow();
                        _userMainWindow.Show();
                        _userMainWindow = new ClientWindow.UserMainWindow();
                    }
                    else if (RestService.Instance.UserType == "Client")
                    {
                        _userMainWindow = new ClientWindow.UserMainWindow();
                    }
                    else if (RestService.Instance.UserType == "Vet")
                    {
                        _userMainWindow = new AdminWindow.MainWindow();
                    }                
                    _userMainWindow?.Show();

                    Close();
                }
                else
                {
                    if (RestService.Instance.ResponseMessage == "Incorrect credentials.")
                    {
                        ErrorLabel.Content = "Błędne dane logowania!";
                        ErrorLabel.Visibility = Visibility.Visible;
                    }
                    else if (RestService.Instance.ResponseMessage == "User has been blocked for 5 minutes due to many failed login attempts.")
                    {
                        ErrorLabel.Content = "Konto zostało zablokowane na 5 minut z powodu zbyt wielu nieudanych prób logowania.";
                        ErrorLabel.Visibility = Visibility.Visible;
                    }
                    else if (RestService.Instance.ResponseMessage == "Your password has expired. Change it first.")
                    {
                        ErrorLabel.Content = "Hasło nie było zmieniane przez 30 dni. Prosze zgłosić się do administratora.";
                        ErrorLabel.Visibility = Visibility.Visible;
                        //System.Windows.MessageBox.Show("Hasło nie było zmieniane przez 30 dni. Prosze zmienić hasło.");
                        //LoginChangePass window = new LoginChangePass()
                        //{
                        //    Owner = this,
                        //    WindowStartupLocation = WindowStartupLocation.CenterOwner
                        //};
                        //window.ShowDialog();
                        
                    }
                    else if((RestService.Instance.ResponseMessage == "User has been blocked due to many failed login attempts. Contact the administrator to unlock the account."))
                    {
                        ErrorLabel.Content = "Konto zostało zablokowane. Prosze zgłosić się do administratora.";
                        ErrorLabel.Visibility = Visibility.Visible;

                    }
                }
            }
        }

        private void CreateAccountButton_Click(object sender, RoutedEventArgs e)
        {
            Window registerWindow = new CreateUserWindow();
            registerWindow.ShowDialog();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}