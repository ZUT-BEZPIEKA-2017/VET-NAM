﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;

namespace WET_NAM_APP.MAIN_Login_Register
{
    public partial class CreateUserWindow : Window
    {
        public CreateUserWindow()
        {
            InitializeComponent();
        }

        private void CreateAccountButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                var data = PrepareData();
                if (RestService.PostInfo2("users/register", data))
                {
                    if (
                        MessageBox.Show("Konto zostało utworzone.\nProszę się zalogować :)", "Konto zostało utworzone",
                            MessageBoxButton.OK, MessageBoxImage.Information) == MessageBoxResult.OK)
                    {
                        Close();
                    }
                }
                else
                {
                    MessageBox.Show("Błąd", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private string PrepareData()
        {
            var userType = "Client";
            string sex;
            if (WomenRadioButton.IsChecked != null && (bool) WomenRadioButton.IsChecked)
            {
                sex = "Female";
            }
            else
            {
                sex = "Male";
            }
            
            var o = new JObject
            {
                {"email", EmailTextBox.Text},
                {"password", PasswordTextBox.Password},
                {"confirmPassword", PasswordRepeatTextBox.Password},
                {"phoneNumber", TelephoneTextBox.Text},
                {"name", NameTextBox.Text},
                {"surname", SurnameTextBox.Text},
                {"sex", sex},
                {"userType", userType}
            };
            return o.ToString();
        }

        private bool ValidateData()
        {
            // TODO: validacja danych wprowadzonych w odpowiednie pola

            #region walidacja wypelnione wszystkie pola

            if (ManRadioButton.IsChecked != null &&
                (WomenRadioButton.IsChecked != null &&
                 (string.IsNullOrWhiteSpace(NameTextBox.Text) || string.IsNullOrWhiteSpace(SurnameTextBox.Text) ||
                  string.IsNullOrWhiteSpace(EmailTextBox.Text) || string.IsNullOrWhiteSpace(TelephoneTextBox.Text) ||
                  string.IsNullOrWhiteSpace(PasswordTextBox.Password) ||
                  string.IsNullOrWhiteSpace(PasswordRepeatTextBox.Password) ||
                  (!(bool) WomenRadioButton.IsChecked && !(bool) ManRadioButton.IsChecked))))
            {
                MessageBox.Show(this, "Należy wypełnić wszystkie pola!", "Błąd - Nie można zarejestrować uźytkownika",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            #region walidacja imie

            var regex = new Regex("^[a-zA-Z]*$");
            var match = regex.Match(NameTextBox.Text);

            if (char.IsLower(NameTextBox.Text[0]) || !match.Success)
            {
                MessageBox.Show(this, "Imię musi zaczynać się wielką literą, tylko litery!",
                    "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            #region walidacja nazwisko

            var regex1 = new Regex("^[a-zA-Z]*$");
            var match1 = regex1.Match(SurnameTextBox.Text);

            if (char.IsLower(SurnameTextBox.Text[0]) || !match1.Success)
            {
                MessageBox.Show(this, "Nazwisko musi zaczynać się wielką literą, tylko litery!",
                    "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            #region walidacja email

            var regex2 =
                new Regex("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            var match2 = regex2.Match(EmailTextBox.Text);
            if (!match2.Success)
            {
                MessageBox.Show(this, "Podano niepoprawny adres E-mail", "Błąd - Nie można zarejestrować uźytkownika",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            #region walidacja numer telefonu

            var valueAsString = TelephoneTextBox.Text;
            // remove '+' character and all white spaces
            valueAsString = valueAsString.Replace("+", string.Empty).Replace(" ", string.Empty);

            foreach (var c in valueAsString)
            {
                if (!char.IsDigit(c))
                {
                    MessageBox.Show(this, "Numer telefonu zawiera niedozwolone znaki!",
                        "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                    return false;
                }
            }

            if (valueAsString.Length != 9 && valueAsString.Length != 11)
            {
                MessageBox.Show(this, "Numer telefonu nie jest odpowiedniej długości!",
                    "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            #region walidacja hasła

            var pass = PasswordTextBox.Password;

            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var MIN_PASS = 8;
            var MAX_PASS = 15;
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(pass) || !hasUpperChar.IsMatch(pass) || !hasNumber.IsMatch(pass) ||
                !hasSymbols.IsMatch(pass) || !(pass.Length >= MIN_PASS && pass.Length <= MAX_PASS))
            {
                MessageBox.Show(this,
                    "Hasło nie spełnia wymogów bezpieczeństwa!\nWymagane: długość 8-15 znaków, zawiera co najmniej jedną cyfrę, literę małą i dużą, oraz znak specjalny.",
                    "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            #region walidacja powtorz hasło

            if (!string.Equals(PasswordTextBox.Password, PasswordRepeatTextBox.Password))
            {
                MessageBox.Show(this, "Hasła nie pasują do siebie!", "Błąd - Nie można zarejestrować uźytkownika",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }

            #endregion

            return true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}