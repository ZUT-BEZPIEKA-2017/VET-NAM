﻿using System;
using System.IO;
using System.Net;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;

namespace WET_NAM_APP.APIclass
{
    public sealed class RestService
    {
        private static RestService c_RestServiceInstance = null;
        private static readonly object m_Lock = new object();

        public string Login { get; set; }
        public string Password { private get; set; }
        public string UserType { get; private set; }
        public string UserID { get; private set; }
        public string ResponseMessage { get; private set; }
        private string Token { get; set; }

        public bool IsToken { get; private set; }
        public bool LastOperationSuccess { get; private set; }

        public static RestService Instance
        {
            get
            {
                lock (m_Lock)
                {
                    if (c_RestServiceInstance == null)
                    {
                        c_RestServiceInstance = new RestService();
                        Instance.IsToken = false;
                    }
                    return c_RestServiceInstance;
                }
            }
        }

        public static bool GetToken()
        {
            var Data = new JObject
            {
                {"email",  Instance.Login},
                {"password", Instance.Password }
            }.ToString();

            var Client = new RestClient();
            Client.BaseUrl = new Uri(Konstanty.Address);

            var Request = new RestRequest("/api/users/token", Method.POST);

            try
            {
                Request.RequestFormat = DataFormat.Json;
                Request.AddHeader("Content-Type", "application/json");

                object JSON = JsonConvert.DeserializeObject(Data);

                Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                IRestResponse Response = Client.Execute(Request);
                dynamic ResponseToJson = JsonConvert.DeserializeObject(Response.Content);

                Instance.ResponseMessage = ResponseToJson.value;

                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    Instance.Token = ResponseToJson.token;
                    Instance.UserID = ResponseToJson.userId;
                    Instance.UserType = ResponseToJson.userType;
                    
                    Instance.IsToken = true;
                    Instance.LastOperationSuccess = true;
                    return true;
                }
                else
                {
                    Instance.IsToken = false;
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            catch
            {
                Instance.IsToken = false;
                Instance.LastOperationSuccess = false;
                return false;
            }
        }

        public static string GetInfo(string Mode)
        {
            if (RestService.Instance.IsToken)
            {
                var Client = new RestClient();
                Client.BaseUrl = new Uri(Konstanty.Address);
                Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                    Instance.Password);

                string FormattedMode = "api/" + Mode + "/";

                var Request = new RestRequest(FormattedMode, Method.GET);
                Request.RequestFormat = DataFormat.Json;
                Request.AddHeader("Authorization", "Bearer " + Instance.Token);
                Request.AddHeader("Accept", "application/json");

                IRestResponse Response = Client.Execute(Request);

                Instance.LastOperationSuccess = true;
                return Response.Content;
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return "Brak tokenu!";
            }
        }

        public static bool PostInfo(string Mode, string Data)
        {
            if (Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                        Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode, Method.POST);
                    Request.RequestFormat = DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + Instance.Token);
                    Request.AddHeader("Content-Type", "application/json");

                    object JSON = JsonConvert.DeserializeObject(Data);
                    //Request.AddBody(JSON);

                    Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return false;
            }
        }

        public static bool PostInfo2(string Mode, string Data)
        {
            var Client = new RestClient();
            Client.BaseUrl = new Uri(Konstanty.Address);
            Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                Instance.Password);

            string FormattedMode = "api/" + Mode + "/";

            var Request = new RestRequest(FormattedMode, Method.POST);

            if (RestService.Instance.IsToken)
            {
                Request.AddHeader("Authorization", "Bearer " + Instance.Token);
            }
            try
                {
                    
                    Request.RequestFormat = DataFormat.Json;
                    
                    Request.AddHeader("Content-Type", "application/json");

                    object JSON = JsonConvert.DeserializeObject(Data);
                    //Request.AddBody(JSON);

                    Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
        


        public static bool DelInfo(string Mode, string Data)
        {
            if (Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                        Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode + Data, Method.DELETE);
                    Request.RequestFormat = DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + Instance.Token);


                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return false;
            }
        }

        public static bool PutInfo(string Mode, string Data, string ID)
        {
            if (Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                        Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode + ID, Method.PUT);
                    Request.RequestFormat = DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + Instance.Token);
                    Request.AddHeader("Content-Type", "application/json");

                    object JSON = JsonConvert.DeserializeObject(Data);
                    //Request.AddBody(JSON);

                    Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return false;
            }
        }

        public static string GetInfoid(string Mode,string id)
        {
            if (RestService.Instance.IsToken)
            {
                var Client = new RestClient();
                Client.BaseUrl = new Uri(Konstanty.Address);
                Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                    Instance.Password);

                string FormattedMode = "api/" + Mode + "/"+id;

                var Request = new RestRequest(FormattedMode, Method.GET);
                Request.RequestFormat = DataFormat.Json;
                Request.AddHeader("Authorization", "Bearer " + Instance.Token);
                Request.AddHeader("Accept", "application/json");

                IRestResponse Response = Client.Execute(Request);

                Instance.LastOperationSuccess = true;
                return Response.Content;
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return "Brak tokenu!";
            }
        }

        public static bool DelInfoUser(string Mode, string Data,string truefalse)
        {
            if (Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                        Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode + Data+"/"+truefalse, Method.DELETE);
                    Request.RequestFormat = DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + Instance.Token);


                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return false;
            }
        }

        public static bool PutInfo(string Mode, string id)
        {
            if (Instance.IsToken)
            {
                try
                {
                    var Client = new RestClient();
                    Client.BaseUrl = new Uri(Konstanty.Address);
                    Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                        Instance.Password);

                    string FormattedMode = "api/" + Mode + "/";

                    var Request = new RestRequest(FormattedMode + id, Method.PUT);
                    Request.RequestFormat = DataFormat.Json;
                    Request.AddHeader("Authorization", "Bearer " + Instance.Token);


                    IRestResponse Response = Client.Execute(Request);

                    if (Response.StatusCode == HttpStatusCode.OK)
                    {
                        Instance.LastOperationSuccess = true;
                        return true;
                    }
                    else
                    {
                        Instance.LastOperationSuccess = false;
                        return false;
                    }
                }
                catch
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            else
            {
                Instance.LastOperationSuccess = false;
                return false;
            }
        }
        public static bool PostInfo3(string Mode, string Data, string Id)
        {
            var Client = new RestClient();
            Client.BaseUrl = new Uri(Konstanty.Address);
            Client.Authenticator = new HttpBasicAuthenticator(Instance.Login,
                Instance.Password);

            string FormattedMode = "api/" + Mode + "/"+Id;

            var Request = new RestRequest(FormattedMode, Method.POST);

            if (RestService.Instance.IsToken)
            {
                Request.AddHeader("Authorization", "Bearer " + Instance.Token);
            }
            try
            {

                Request.RequestFormat = DataFormat.Json;

                Request.AddHeader("Content-Type", "application/json");

                object JSON = JsonConvert.DeserializeObject(Data);
                //Request.AddBody(JSON);

                Request.AddParameter("application/json; charset=utf-8", JSON, ParameterType.RequestBody);

                IRestResponse Response = Client.Execute(Request);

                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    Instance.LastOperationSuccess = true;
                    return true;
                }
                else
                {
                    Instance.LastOperationSuccess = false;
                    return false;
                }
            }
            catch
            {
                Instance.LastOperationSuccess = false;
                return false;
            }
        }
    }
}