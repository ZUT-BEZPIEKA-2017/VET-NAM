﻿namespace WET_NAM_APP.AdminWindow
{
    public class pet
    {

        public string id { get; set; }
        
        public string name { get; set; }
        public string owner { get; set; }
        public string breed { get; set; }
        public string description { get; set; }
        public string sex { get; set; }
        public bool vaccinated { get; set; }
        public string address { get; set; }

    }

    public class petview
    {
        public string name { get; set; }
        public string owner { get; set; }

    }


}