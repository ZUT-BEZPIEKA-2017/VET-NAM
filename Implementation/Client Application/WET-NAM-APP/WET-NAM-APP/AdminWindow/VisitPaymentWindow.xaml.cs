﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;


namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy VisitPaymentWindow.xaml
    /// </summary>
    public partial class VisitPaymentWindow : Window
    {

        
        
        payments selectedpayment;
        public VisitPaymentWindow( client tempclient, visit tempvisit,payments temppayment)
        {
            InitializeComponent();
             
             selectedpayment= temppayment;
            ClientTextBox.Text = tempclient.name + " " + tempclient.surname;

            if(string.Compare( temppayment.status, "Completed") == 0)
            {
                StatusTextBox.Text = "Opłacone";
            }
            else
            {
                StatusTextBox.Text = "Nieopłacone";
            }
            if (temppayment.pymentDate == null)
            {
                DateTextBox.Text = "";
            }
            else
            {
                DateTextBox.Text = temppayment.pymentDate;
            }
            CostTextBox.Text = temppayment.amount.ToString();



        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            selectedpayment.status = "Completed";
            selectedpayment.pymentDate = System.DateTime.Now.ToString();
            string post = JsonConvert.SerializeObject(selectedpayment);

            if (RestService.PutInfo("payments/update", post, selectedpayment.id))
            {
                
                

                    Close();
               
            }
            else
                MessageBox.Show("wystąpił błąd podczas edytowania opłat");

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
