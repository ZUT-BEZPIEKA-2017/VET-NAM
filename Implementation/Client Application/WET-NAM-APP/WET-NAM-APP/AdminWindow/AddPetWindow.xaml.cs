﻿using System;
using System.Windows;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;


namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy AddPetWindow.xaml
    /// </summary>
    public partial class AddPetWindow : Window
    {
        public pet temppet = new pet();
        public bool addclick;


        public AddPetWindow(client tempclient)
        {
            InitializeComponent();
            Title = "dodaj zwierzę";


            OwnerTextBox.Text = tempclient.id;
           temppet.owner = tempclient.id;

        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {


            if(string.Compare(temppet.owner, OwnerTextBox.Text) == 0)
            {
                if (!(string.IsNullOrWhiteSpace(OwnerTextBox.Text) || string.IsNullOrWhiteSpace(NameTextBox.Text) ||
                  string.IsNullOrEmpty(BreedTextBox.Text) || string.IsNullOrWhiteSpace(AddressTextBox.Text) || string.IsNullOrEmpty(SexComboBox.SelectedValue.ToString()) || string.IsNullOrEmpty(vaccinatedComboBox.SelectedValue.ToString())))
                {

                    temppet.name = NameTextBox.Text;
                    temppet.sex = SexComboBox.SelectedValue.ToString();
                    temppet.breed = BreedTextBox.Text;
                    temppet.address = AddressTextBox.Text;
                    if (vaccinatedComboBox.SelectedIndex == 0) temppet.vaccinated = true;
                    else temppet.vaccinated = false;
                    if (DescriptionTextBox.Text == null)
                        temppet.description = "";
                    else temppet.description = DescriptionTextBox.Text;

                    string postpet = JsonConvert.SerializeObject(temppet);

                    if (RestService.PostInfo2("pets/add", postpet))
                    {
                        addclick = true;
                        Close();
                    }
                    else
                        MessageBox.Show("wystąpił błąd podczas dodawania rekordu");

                    
                }
                else
                {
                    MessageBox.Show("wypełnij wszystkie pola");
                }
            }
            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            addclick = false;
            Close();
        }
    }
}