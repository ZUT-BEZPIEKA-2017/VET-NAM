﻿using System;
using System.Windows;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;

namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy EditPetWindow.xaml
    /// </summary>
    public partial class EditPetWindow : Window
    {

        public pet temppet = new pet();
        string tempclient;
        public bool addclick;
        public EditPetWindow( pet editpet)
        {
            InitializeComponent();
            Title = "edytuj zwierzę";
            OwnerTextBox.Text = editpet.owner;
            IdTextBox.Text = editpet.id;
            NameTextBox.Text = editpet.name;
            AddressTextBox.Text = editpet.address;
            if (editpet.vaccinated == true) vaccinatedComboBox.SelectedIndex = 0;
            else vaccinatedComboBox.SelectedIndex = 1;
            if (editpet.sex == "Male")
                SexComboBox.SelectedIndex = 0;
            else SexComboBox.SelectedIndex = 1;
            BreedTextBox.Text = editpet.breed;
            DescriptionTextBox.Text = editpet.description;
            temppet = editpet;
            tempclient = editpet.owner;

        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {

            if (string.Compare(temppet.owner,tempclient) == 0)
            {
                if (string.Compare(temppet.id,IdTextBox.Text)==0)
                {


                    if (!(string.IsNullOrWhiteSpace(OwnerTextBox.Text) || string.IsNullOrWhiteSpace(NameTextBox.Text) || string.IsNullOrWhiteSpace(temppet.id) ||
                      string.IsNullOrEmpty(BreedTextBox.Text) || string.IsNullOrEmpty(vaccinatedComboBox.SelectedValue.ToString()) || string.IsNullOrWhiteSpace(AddressTextBox.Text) || ((string.Compare(SexComboBox.SelectedValue.ToString(),"Male")!=0 && string.Compare(SexComboBox.SelectedValue.ToString(), "Female")!=0))))
                    {

                        temppet.name = NameTextBox.Text;
                        temppet.sex = SexComboBox.SelectedValue.ToString();
                        temppet.breed = BreedTextBox.Text;
                        temppet.address = AddressTextBox.Text;
                        if (vaccinatedComboBox.SelectedIndex == 0) temppet.vaccinated = true;
                        else temppet.vaccinated = false;
                        if (DescriptionTextBox.Text == null)
                            temppet.description = "";
                        else temppet.description = DescriptionTextBox.Text;

                        string postpet = JsonConvert.SerializeObject(temppet);
                        //MessageBox.Show(postpet);
                        if (RestService.PutInfo("pets/update", postpet,temppet.id))
                        {
                            addclick = true;
                            Close();
                        }
                        else
                            MessageBox.Show("wystąpił błąd podczas edytowania rekordu");


                    }
                    else
                    {
                        MessageBox.Show("wypełnij wszystkie pola");
                    }
                }
                else MessageBox.Show("błąd id zwierzaka");
            }
            else MessageBox.Show("błąd id właściciela");

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            addclick = false;
            Close();
        }
    }
}