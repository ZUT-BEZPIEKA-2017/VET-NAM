﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;


namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        


        List<client> employelist = new List<client>();
        List<client> clientlist = new List<client>();
        List<pet> petlist = new List<pet>();
        List<petview> petshow = new List<petview>();
        List<visit> visitlist = new List<visit>();
        List<visit> visithistory = new List<visit>();
        List<visit> visitinday = new List<visit>();
        List<visit> visitindayhistory = new List<visit>();


        //do testow visitrefresh2
        List<VisitsView> visitlistshow = new List<VisitsView>();
        List<VisitsView> visithistoryshow = new List<VisitsView>();
        List<VisitsView> visitindayshow = new List<VisitsView>();
        List<VisitsView> visitindayhistoryshow = new List<VisitsView>();

        client[] allusers;
        pet[] allpet;
        visit[] allvisit;

        public MainWindow()
        {
            InitializeComponent();
            

            visitinday.Clear();
            visithistory.Clear();

            string jsonallusers = RestService.GetInfo("users");
            allusers = JsonConvert.DeserializeObject<client[]>(jsonallusers);
            foreach(client temp in allusers)
            {
                if (temp.usertype == "Client")
                {
                    clientlist.Add(temp);
                }
                if (temp.usertype == "Vet")
                {
                    employelist.Add(temp);
                }
            }

            //string jsonallpet = RestService.GetInfo("pets");
            //allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            //petlist.Clear();
            //foreach (pet temp in allpet)
            //{
            //    if (ClientListView.SelectedIndex == -1)
            //    {
            //        petlist.Add(temp);
            //    }


            //}
            //PetListView.ItemsSource = petlist;
            petrefresh();
           
            EmployeesListView.ItemsSource = employelist;
            ClientListView.ItemsSource = clientlist;
            
            VisitHistoryListView.ItemsSource = visithistory;
            VisitInDayListView.ItemsSource = visitlist;
            VisitHistoryListView1.ItemsSource = visitindayhistory;
            VisitInDayListView1.ItemsSource = visitinday;

            visitrefresh3();

        }

        private void AddEmployeButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            AddEmployeWindow addempwindow = new AddEmployeWindow()
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            addempwindow.ShowDialog();
            if (addempwindow.addclick)
            {

                string jsonallusers = RestService.GetInfo("users");
                allusers = JsonConvert.DeserializeObject<client[]>(jsonallusers);
                clientlist.Clear();
                employelist.Clear();
                foreach (client temp in allusers)
                {
                    if (temp.usertype == "Client")
                    {
                        clientlist.Add(temp);
                    }
                    if (temp.usertype == "Vet")
                    {
                        employelist.Add(temp);
                    }
                }



            }
            //petlist.Add(window.temppet);



            ICollectionView view = CollectionViewSource.GetDefaultView(clientlist);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(employelist);
            view.Refresh();
        }

        private void DeleteEmployeButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            if (EmployeesListView.SelectedIndex >= EmployeesListView.Items.Count) EmployeesListView.SelectedIndex = -1;
            int index = EmployeesListView.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("nie wybrano klienta");
                return;
            }

            client selectedclient = EmployeesListView.Items.GetItemAt(index) as client;
            if (RestService.DelInfoUser("users/delete", selectedclient.id, "false"))
            {
                MessageBox.Show("użytkownik usunięty");
            }
            else
            {
                MessageBox.Show("błąd podczas usuwania");
            }


            usersrefresh();
            ICollectionView view = CollectionViewSource.GetDefaultView(clientlist);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(employelist);
            view.Refresh();
        }

        private void DeleteClientButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            int index = ClientListView.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("nie wybrano klienta");
                return;
            }

            client selectedclient = ClientListView.Items.GetItemAt(index) as client;
            if(RestService.DelInfoUser("users/delete", selectedclient.id, "true"))
            {
                MessageBox.Show("użytkownik usunięty");
            }
            else
            {
                MessageBox.Show("błąd podczas usuwania");
            }
            

            usersrefresh();


            petlist.Clear();

            ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            view.Refresh();

        }

        private void AddClientButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            AddClientWindow addcliwindow = new AddClientWindow()
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            addcliwindow.ShowDialog();

            if (addcliwindow.addclick)
            {

                usersrefresh();



            }
           
        }

        private void EmployeesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void ClientListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RestService.GetToken();
            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            PetListView.SelectedIndex = -1;
            int index = ClientListView.SelectedIndex;
            if (index == -1)
            {
                
                return;
            }

            petrefresh();
            //client selectedclient = ClientListView.Items.GetItemAt(index) as client;
            //string jsonallpet = RestService.GetInfo("pets");
            //allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            //petlist.Clear();
            //if (allpet != null)
            //{


            //    foreach (pet temp in allpet)
            //    {
            //        if (string.Compare(selectedclient.id, temp.owner) == 0)
            //        {
            //            petlist.Add(temp);
            //        }


            //    }
            //}

            //ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            //view.Refresh();

        }


        private void EditClientButton_Click(object sender, RoutedEventArgs e)
        {

            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            int index = ClientListView.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("nie wybrano klienta");
                return;
            }
            client tempclient = ClientListView.Items.GetItemAt(index) as client;
            EditClientWindow window = new EditClientWindow(tempclient)
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            window.ShowDialog();


            if (window.addclick)
            {

                usersrefresh();



            }
            
        }

        private void EditEmployeButton_Click(object sender, RoutedEventArgs e)
        {
            int index = EmployeesListView.SelectedIndex;
            if (index == -1)
            {
                System.Windows.MessageBox.Show("error");
                return;
            }
            EditEmployeWindow editempwindow = new EditEmployeWindow()
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            editempwindow.ShowDialog();


            usersrefresh();
        }

        private void AddPetButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            int index = ClientListView.SelectedIndex;
            if (index == -1)
            {
                System.Windows.MessageBox.Show("nie wybrano klienta");
                return;
            }
            client tempclient = ClientListView.Items.GetItemAt(index) as client;
            AddPetWindow window = new AddPetWindow(tempclient)
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            window.ShowDialog();

            petrefresh();

            //if (window.addclick)
            //{
            //    client selectedclient = ClientListView.Items.GetItemAt(index) as client;
            //    string jsonallpet = RestService.GetInfo("pets");
            //    allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            //    petlist.Clear();
            //    foreach (pet temp in allpet)
            //    {
            //        if (string.Compare(selectedclient.id, temp.owner) == 0)
            //        {
            //            petlist.Add(temp);
            //        }


            //    }

                

            //}
            //ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            //view.Refresh();
        }

        private void EditPetButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            if (PetListView.SelectedIndex >= PetListView.Items.Count) PetListView.SelectedIndex = -1;
            
            int index2 = PetListView.SelectedIndex;
            //int index = ClientListView.SelectedIndex;
            //if (index == -1 )
            //{
            //    MessageBox.Show("nie wybrano klienta");
            //    return;
            //}
            //client clienttoedit = ClientListView.Items.GetItemAt(index) as client;
            if (index2 == -1)
            {
                MessageBox.Show("nie wybrano zwierzaka");
                return;
            }
            pet pettoedit = petlist[index2];
            
            EditPetWindow window = new EditPetWindow(pettoedit)
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            window.ShowDialog();



            petrefresh();
            //if (window.addclick)
            //{
            //    client selectedclient = ClientListView.Items.GetItemAt(ClientListView.SelectedIndex) as client;
            //    string jsonallpet = RestService.GetInfo("pets");
            //    allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            //    petlist.Clear();
            //    foreach (pet temp in allpet)
            //    {
            //        if (string.Compare(selectedclient.id, temp.owner) == 0)
            //        {
            //            petlist.Add(temp);
            //        }
            //    }
            //}
            //ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            //view.Refresh();
        }

        private void DeletePetButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            if (PetListView.SelectedIndex >= PetListView.Items.Count) PetListView.SelectedIndex = -1;
            
            int index2 = PetListView.SelectedIndex;
            
            if (index2 == -1)
            {
                MessageBox.Show("nie wybrano zwierzaka");
                return;
            }
            pet del = petlist[index2];

            
            if (RestService.DelInfo("pets/delete", del.id))
            {
            }
            else
                MessageBox.Show("wystąpił błąd podczas usuwania rekordu");

            petrefresh();
            //client selectedclient = ClientListView.Items.GetItemAt(ClientListView.SelectedIndex) as client;
            //string jsonallpet = RestService.GetInfo("pets");
            //allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            //petlist.Clear();
            //foreach (pet temp in allpet)
            //{
            //    if (string.Compare(selectedclient.id, temp.owner) == 0)
            //    {
            //        petlist.Add(temp);
            //    }


            //}
            //ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            //view.Refresh();
        }

        private void PetListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RestService.GetToken();
            if (PetListView.SelectedIndex >= PetListView.Items.Count) PetListView.SelectedIndex = -1;
            int index = PetListView.SelectedIndex;
            if (index == -1)
            {
                return;
            }
            pet temp = petlist[index];
            PetSexTextBox.Text = temp.sex;
            PetBreedTextBox.Text = temp.breed;
            PetDescriptionTextBox.Text = temp.description;
            AddressTextBox.Text = temp.address;
            if (temp.vaccinated == true)
                vaccinatedTextBox.Text = "Tak";
            else vaccinatedTextBox.Text = "Nie";



        }

        private void visitclientcalendar_change(object sender, SelectionChangedEventArgs e)
        {

            visitrefresh3();
        }



        private void VisitClientAddButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            int index = ClientListView.SelectedIndex;
            int index2 = PetListView.SelectedIndex;
            if (index == -1)
            {
                MessageBox.Show("nie wybrano klienta");
                return;
            }
            if (index2 == -1)
            {
                MessageBox.Show("nie wybrano zwierzaka");
                return;
            }
            pet del = petlist[index2];
            
            client selectedclient = ClientListView.Items.GetItemAt(index) as client;
            client selectedvet = EmployeesListView.Items.GetItemAt(0) as client;
            
            string guid= System.Guid.NewGuid().ToString();
            //MessageBox.Show(guid);
            payments addpayment = new payments() { amount = 10, status = "InProgress", user = selectedclient.id, id=guid };
            visit addvisit = new visit() { user = selectedclient.id, pet = del.id, vet = selectedvet.id, startDate = System.DateTime.Today, duration= System.TimeSpan.Parse("00:10:00"), payment=guid, description="" }   ;
            string postvisit = JsonConvert.SerializeObject(addvisit);
            MessageBox.Show(postvisit);
            if (RestService.PostInfo2("visits/add", postvisit)) {
                string postpayment = JsonConvert.SerializeObject(addpayment);
                if (RestService.PostInfo2("payments/add", postpayment)) { }
                else MessageBox.Show("wystąpił błąd podczas dodawania oplat");

            }
            else MessageBox.Show("wystąpił błąd podczas dodawania wizyty");


            visitrefresh3();

            

        }



        private void FiltrClientTextBox_change(object sender, TextChangedEventArgs e)
        {
            string filtr = FiltrClientTextBox.Text;
            string compare;
            ClientListView.SelectedIndex = -1;
            PetListView.SelectedIndex = -1;
            clientlist.Clear();
            foreach (client temp in allusers)
            {
                if (temp.usertype == "Client")
                {
                    if (filtr.Length > 0)
                    {
                        if (temp.email != null) {
                            if (temp.email.Length > filtr.Length)
                                compare = temp.email.Substring(0, filtr.Length);
                            else compare = temp.email;
                            if (compare != null && string.Compare(filtr, compare, true) == 0)
                            {
                                clientlist.Add(temp);
                                continue;
                            }
                        }
                        if (temp.name != null) {
                            if (temp.name.Length > filtr.Length)
                                compare = temp.name.Substring(0, filtr.Length);
                            else compare = temp.name;
                            if (compare != null && string.Compare(filtr, compare, true) == 0)
                            {
                                clientlist.Add(temp);
                                continue;
                            }
                        }
                        if (temp.surname != null) {
                            if (temp.surname.Length > filtr.Length)
                                compare = temp.surname.Substring(0, filtr.Length);
                            else compare = temp.surname;
                            if (compare != null && string.Compare(filtr,compare,true) == 0) 
                            {
                                clientlist.Add(temp);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        clientlist.Add(temp);
                    }


                }

            }
            ClientListView.ItemsSource = clientlist;
            ICollectionView view = CollectionViewSource.GetDefaultView(clientlist);
            view.Refresh();
        }

        private void FiltrEmployeTextBox_change(object sender, TextChangedEventArgs e)
        {

            string filtr = FiltrEmployeTextBox.Text;
            string compare;
            
            employelist.Clear();

            foreach (client temp in allusers)
            {
                if (temp.usertype == "Vet")
                {
                    if (filtr.Length > 0)
                    {
                        if (temp.email != null)
                        {
                            if (temp.email.Length > filtr.Length)
                                compare = temp.email.Substring(0, filtr.Length);
                            else compare = temp.email;
                            if (compare != null && string.Compare(filtr, compare, true) == 0)
                            {
                                employelist.Add(temp);
                                continue;
                            }
                        }
                        if (temp.name != null)
                        {
                            if (temp.name.Length > filtr.Length)
                                compare = temp.name.Substring(0, filtr.Length);
                            else compare = temp.name;
                            if (compare != null && string.Compare(filtr, compare, true) == 0)
                            {
                                employelist.Add(temp);
                                continue;
                            }
                        }
                        if (temp.surname != null)
                        {
                            if (temp.surname.Length > filtr.Length)
                                compare = temp.surname.Substring(0, filtr.Length);
                            else compare = temp.surname;
                            if (compare != null && string.Compare(filtr, compare, true) == 0) 
                            {
                                employelist.Add(temp);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        employelist.Add(temp);
                    }


                }

            }
            EmployeesListView.ItemsSource = employelist;
            ICollectionView view = CollectionViewSource.GetDefaultView(employelist);
            view.Refresh();
        }

        private void VisitInDayListView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RestService.GetToken();
            if (VisitInDayListView1.SelectedIndex >= VisitInDayListView1.Items.Count) VisitInDayListView1.SelectedIndex = -1;
            if (VisitInDayListView1.SelectedIndex != -1)
            {
                visit selectedvisit = visitinday[VisitInDayListView1.SelectedIndex];// as visit;
                client selectedclient = new client();
                client selectedvet = new client();
                pet selectedpet = new pet();
                payments selectedpayment = new payments();

                if (selectedvisit.user != null)
                {
                    string jsonall = RestService.GetInfoid("users",selectedvisit.user);
                    if (string.Compare(jsonall, "[]") != 0)
                    {
                        client[] allu = JsonConvert.DeserializeObject<client[]>(jsonall);
                    
                        selectedclient = allu[0];
                    }
                }
                if (selectedvisit.vet != null)
                {
                    string jsonall = RestService.GetInfoid("users",selectedvisit.vet);
                    if (string.Compare(jsonall, "[]") != 0)
                    {
                        client[] allu = JsonConvert.DeserializeObject<client[]>(jsonall);

                        selectedvet = allu[0];
                    }
                }
                if (selectedvisit.pet != null)
                {
                    string jsonall = RestService.GetInfoid("pets",selectedvisit.pet);
                    if (string.Compare(jsonall, "[]") != 0)
                    {
                        pet[] allu = JsonConvert.DeserializeObject<pet[]>(jsonall);
                        selectedpet = allu[0];
                        
                    }
                }
                if (selectedvisit.payment != null)
                {
                    string jsonall = RestService.GetInfoid("payments",selectedvisit.payment);
                    if (string.Compare(jsonall, "[]") != 0)
                    {
                        //MessageBox.Show(jsonall);
                        payments[] allu = JsonConvert.DeserializeObject<payments[]>(jsonall);
                        selectedpayment = allu[0];
                    }
                }

                if ((selectedvet.id == null) || (selectedclient.id == null) || (selectedpet.id == null) || (selectedvisit.id == null) || (selectedpayment.id == null))
                {
                    if (selectedvet.id == null) MessageBox.Show("nie znaleziono vet");
                    if (selectedclient.id == null) MessageBox.Show("nie znaleziono client");
                    if (selectedpet.id == null) MessageBox.Show("nie znaleziono pet");
                    if (selectedvisit.id == null) MessageBox.Show("nie znaleziono visit");
                    if (selectedpayment.id == null) MessageBox.Show("nie znaleziono payment");


                }
                else
                {
                    VisitActualWindow window = new VisitActualWindow(selectedvet, selectedclient, selectedpet, selectedvisit, selectedpayment)
                    {
                        Owner = this,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner
                    };
                    window.ShowDialog();
                }


                visitrefresh3();

            }
            

        }



        private void petrefresh()
        {

            client tempclient = new client();
            string jsonallusers = RestService.GetInfo("users");
            allusers = JsonConvert.DeserializeObject<client[]>(jsonallusers);
            string jsonallpet = RestService.GetInfo("pets");
            allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            petlist.Clear();
            petshow.Clear();
            if (allpet != null)
            {
                if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
                if (ClientListView.SelectedIndex != -1)
                {
                    tempclient = ClientListView.Items.GetItemAt(ClientListView.SelectedIndex) as client;
                   
                }
                
                foreach (pet temp in allpet)
                {
                    if (ClientListView.SelectedIndex == -1)
                    {
                        petlist.Add(temp);
                        if (allusers != null)
                        {

                            foreach (client tempowner in allusers)
                            {
                                if (string.Compare(temp.owner, tempowner.id) == 0)
                                {
                                    petshow.Add(new petview() { name = temp.name, owner = tempowner.surname });
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        if(tempclient.id!=null)
                            if (string.Compare(tempclient.id, temp.owner) == 0)
                            {
                                petlist.Add(temp);
                                petshow.Add(new petview() { name = temp.name, owner = tempclient.surname });

                            }
                    }
                    
                }
            }



            PetListView.ItemsSource = petshow;

            ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(petshow);
            view.Refresh();

        }

        private void visitrefresh3() // działa
        {


            List<client> allcli = new List<client>();
            List<pet> allpet = new List<pet>();
            List<payments> allpay = new List<payments>();

            allcli.Clear();
            allpet.Clear();
            allpay.Clear();

            string jsonall = RestService.GetInfo("users");
            if (string.Compare(jsonall, "[]") != 0)
            {
                client[] allu = JsonConvert.DeserializeObject<client[]>(jsonall);
                foreach (client temp in allu)
                    allcli.Add(temp);
            }

            jsonall = RestService.GetInfo("pets");
            if (string.Compare(jsonall, "[]") != 0)
            {
                pet[] allu = JsonConvert.DeserializeObject<pet[]>(jsonall);
                foreach (pet temp in allu)
                    allpet.Add(temp);
            }

            jsonall = RestService.GetInfo("payments");
            if (string.Compare(jsonall, "[]") != 0)
            {
                payments[] allu = JsonConvert.DeserializeObject<payments[]>(jsonall);
                foreach (payments temp in allu)
                    allpay.Add(temp);
            }


            string paytemp;
            string jsonallvisit = RestService.GetInfo("visits");
            allvisit = JsonConvert.DeserializeObject<visit[]>(jsonallvisit);
            visithistory.Clear();
            visitinday.Clear();
            visitlist.Clear();
            visitindayhistory.Clear();
            visitindayhistoryshow.Clear();
            visithistoryshow.Clear();
            visitindayshow.Clear();
            visitlistshow.Clear();
            foreach (visit temp in allvisit)
            {

                client selectedclient = new client() { name = null, id = null };
                client selectedvet = new client() { name = null, id = null };

                pet selectedpet = new pet() { name = null, id = null };
                payments selectedpayment = new payments() { pymentDate = "", status = "", id = null };

                if (temp.user != null)
                {
                    if((selectedclient = allcli.Find(x => x.id.Equals(temp.user)))==null) selectedclient = new client() { name = null, id = null }; ;
                }

                if (temp.vet != null)
                {
                    if((selectedvet = allcli.Find(x => x.id.Equals(temp.vet)))==null) selectedvet = new client() { name = null, id = null };
                }
                if (temp.pet != null)
                {
                    if((selectedpet = allpet.Find(x => x.id.Equals(temp.pet))) == null) selectedpet = new pet() { name = null, id = null };
                }
                if (temp.payment != null)
                {
                    if((selectedpayment = allpay.Find(x => x.id.Equals(temp.payment))) == null) selectedpayment = new payments() { pymentDate = "", status = "", id = null };
                }

                if (string.Compare(selectedpayment.status, "Completed") == 0)
                    paytemp = "Tak";
                else paytemp = "Nie";

                //new VisitsView() { client = selectedclient.name, pet = selectedpet.name, vet = selectedvet.name, pay = paytemp };


                if (temp.endDate != null && temp.endDate < System.DateTime.Now && temp.endDate.Year != 0001)
                {
                    visithistory.Add(temp);
                    visithistoryshow.Add(new VisitsView() { client = selectedclient.name, pet = selectedpet.name, vet = selectedvet.name, pay = paytemp, time = temp.endDate.ToShortDateString() });

                }
                // zmienic na ==
                if (temp.startDate.Date == System.DateTime.Today.Date)
                {

                    if ((temp.endDate < System.DateTime.Now))
                    {
                        visitindayhistory.Add(temp);
                        visitindayhistoryshow.Add(new VisitsView() { client = selectedclient.name, pet = selectedpet.name, vet = selectedvet.name, pay = paytemp, time = selectedpayment.pymentDate });
                    }
                    else
                    {
                        visitinday.Add(temp);
                        visitindayshow.Add(new VisitsView() { client = selectedclient.name, pet = selectedpet.name, vet = selectedvet.name, pay = paytemp, time = temp.startDate.ToShortTimeString() });
                    }

                }
                if (visitclientcalendar.SelectedDate != null)
                {
                    if (temp.startDate.Date == visitclientcalendar.SelectedDate.Value.Date)
                    {
                        visitlist.Add(temp);
                        visitlistshow.Add(new VisitsView() { client = selectedclient.name, pet = selectedpet.name, vet = selectedvet.name, pay = paytemp, time = selectedpayment.pymentDate });
                        // test MessageBox.Show(temp.startDate.Date.ToShortDateString() + "   " + visitclientcalendar.SelectedDate.Value.Date.ToShortDateString());
                    }
                }
                else
                {
                    if (temp.startDate.Date >= System.DateTime.Today.Date)
                    {
                        visitlist.Add(temp);
                        visitlistshow.Add(new VisitsView() { client = selectedclient.name, pet = selectedpet.name, vet = selectedvet.name, pay = paytemp, time = selectedpayment.pymentDate });
                    }
                }





            }
            VisitHistoryListView.ItemsSource = visithistoryshow;
            VisitInDayListView.ItemsSource = visitlistshow;
            VisitHistoryListView1.ItemsSource = visitindayhistoryshow;
            VisitInDayListView1.ItemsSource = visitindayshow;
            ICollectionView view = CollectionViewSource.GetDefaultView(visithistoryshow);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(visitindayshow);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(visitlistshow);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(visitindayhistoryshow);
            view.Refresh();

        }

        private void usersrefresh()
        {
            string jsonallusers = RestService.GetInfo("users");
            allusers = JsonConvert.DeserializeObject<client[]>(jsonallusers);
            clientlist.Clear();
            employelist.Clear();
            foreach (client temp in allusers)
            {
                if (temp.usertype == "Client")
                {
                    clientlist.Add(temp);
                }
                if (temp.usertype == "Vet")
                {
                    employelist.Add(temp);
                }
            }
            ICollectionView view = CollectionViewSource.GetDefaultView(clientlist);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(employelist);
            view.Refresh();
        }

        private void VisitPaymentListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RestService.GetToken();
            if (VisitHistoryListView1.SelectedIndex >= VisitHistoryListView1.Items.Count) VisitHistoryListView1.SelectedIndex = -1;
            if (VisitHistoryListView1.SelectedIndex != -1)
            {
                visit selectedvisit = visitindayhistory[VisitHistoryListView1.SelectedIndex];// as visit;
                client selectedclient = new client();
                payments selectedpayment = new payments();

                if (selectedvisit.user != null)
                {
                    string jsonall = RestService.GetInfoid("users", selectedvisit.user);
                    if (string.Compare(jsonall, "[]") != 0)
                    {
                        client[] allu = JsonConvert.DeserializeObject<client[]>(jsonall);

                        selectedclient = allu[0];
                    }
                }
              
                if (selectedvisit.payment != null)
                {
                    string jsonall = RestService.GetInfoid("payments", selectedvisit.payment);
                    if (string.Compare(jsonall, "[]") != 0)
                    {
                        //MessageBox.Show(jsonall);
                        payments[] allu = JsonConvert.DeserializeObject<payments[]>(jsonall);
                        selectedpayment = allu[0];
                    }
                }

                if ( (selectedclient.id == null) || (selectedvisit.id == null) || (selectedpayment.id == null))
                {
                    
                    if (selectedclient.id == null) MessageBox.Show("nie znaleziono client");
                    if (selectedvisit.id == null) MessageBox.Show("nie znaleziono visit");
                    if (selectedpayment.id == null) MessageBox.Show("nie znaleziono payment");
                }
                else
                {
                    VisitPaymentWindow window = new VisitPaymentWindow( selectedclient,  selectedvisit, selectedpayment)
                    {
                        Owner = this,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner
                    };
                    window.ShowDialog();
                }


                visitrefresh3();

            }


        }

        private void LogoutButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show(this, "Czy na pewno chcesz zamknąć sesję i wylogować się z aplikacji?",
                "Potwierdzenie wylogowania", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void FiltrPetTextBox_change(object sender, TextChangedEventArgs e)
        {
            string filtr = FiltrPetTextBox.Text;
            string compare;
            ClientListView.SelectedIndex = -1;
            PetListView.SelectedIndex = -1;
            petlist.Clear();
            petshow.Clear();
            string jsonallusers = RestService.GetInfo("users");
            allusers = JsonConvert.DeserializeObject<client[]>(jsonallusers);
            string jsonallpet = RestService.GetInfo("pets");
            allpet = JsonConvert.DeserializeObject<pet[]>(jsonallpet);
            if (allpet != null)
            {
                foreach (pet temp in allpet)
                {

                    if (filtr.Length > 0)
                    {

                        if (temp.name != null)
                        {
                            if (temp.name.Length > filtr.Length)
                                compare = temp.name.Substring(0, filtr.Length);
                            else compare = temp.name;
                            if (compare != null && string.Compare(filtr, compare, true) == 0)
                            {
                                petlist.Add(temp);
                                if (allusers != null)
                                {

                                    foreach (client tempowner in allusers)
                                    {
                                        if (string.Compare(temp.owner, tempowner.id) == 0)
                                        {
                                            petshow.Add(new petview() { name = temp.name, owner = tempowner.surname });
                                            break;
                                        }
                                    }
                                }
                                continue;
                            }
                        }

                        if (allusers != null)
                        {

                            foreach (client tempowner in allusers)
                            {
                                
                                if (string.Compare(temp.owner, tempowner.id) == 0)
                                {

                                    if (tempowner.surname.Length > filtr.Length)
                                        compare = tempowner.surname.Substring(0, filtr.Length);
                                    else compare = tempowner.surname;
                                    if (compare != null && string.Compare(filtr, compare, true) == 0)
                                    {
                                        petlist.Add(temp);
                                        petshow.Add(new petview() { name = temp.name, owner = tempowner.surname });
                                    }

                                        
                                    break;
                                }
                                
                            }
                        }




                    }
                    else
                    {
                        petlist.Add(temp);
                        if (allusers != null)
                        {

                            foreach (client tempowner in allusers)
                            {
                                if (string.Compare(temp.owner, tempowner.id) == 0)
                                {
                                    petshow.Add(new petview() { name = temp.name, owner = tempowner.surname });
                                    break;
                                }
                            }
                        }
                    }




                }
            }
            PetListView.ItemsSource = petshow;
            ICollectionView view = CollectionViewSource.GetDefaultView(petlist);
            view.Refresh();
            view = CollectionViewSource.GetDefaultView(petshow);
            view.Refresh();
        }

        private void UnlockClientButton_Click(object sender, RoutedEventArgs e)
        {
            if (ClientListView.SelectedIndex >= ClientListView.Items.Count) ClientListView.SelectedIndex = -1;
            int index = ClientListView.SelectedIndex;
            if (index == -1)
            {
                System.Windows.MessageBox.Show("nie wybrano klienta");
                return;
            }
            client tempclient = ClientListView.Items.GetItemAt(index) as client;
            if (!(RestService.PutInfo("users/unlock", tempclient.id))) System.Windows.MessageBox.Show("błąd podczas odblokowywania"); ;




        }

        private void UnlockVetButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeesListView.SelectedIndex >= EmployeesListView.Items.Count) EmployeesListView.SelectedIndex = -1;
            int index = EmployeesListView.SelectedIndex;
            if (index == -1)
            {
                System.Windows.MessageBox.Show("nie wybrano klienta");
                return;
            }
            client tempclient = EmployeesListView.Items.GetItemAt(index) as client;
            if(!(RestService.PutInfo("users/unlock", tempclient.id))) System.Windows.MessageBox.Show("błąd podczas odblokowywania"); 


        }

        private void PasswordChangeButton_Click(object sender, RoutedEventArgs e)
        {
            RestService.GetToken();
            PasswordChangeWindow window = new PasswordChangeWindow()
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            window.ShowDialog();
        }
    }
    
}