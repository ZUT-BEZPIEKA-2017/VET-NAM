﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;


namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy VisitActualWindow.xaml
    /// </summary>
    public partial class VisitActualWindow : Window
    {

        visit tempv;
        payments tempp ;
        public VisitActualWindow(client vet, client cli, pet p , visit v, payments pay)
        {
            InitializeComponent();
            VetTextBox.Text = vet.name + " " + vet.surname;
            ClientTextBox.Text = cli.name + " " + cli.surname;
            PetTextBox.Text = p.name;
            SexTextBox.Text = p.sex;
            BreedTextBox.Text = p.breed;
            PetDescriptionTextBox.Text = p.description;
            CostTextBox.Text = pay.amount.ToString();
            if (v.description != null) VisitDescriptionTextBox.Text = v.description;
             tempv = v;
             tempp = pay;
        }

        private void EndButton_Click(object sender, RoutedEventArgs e)
        {

            double cost;
            tempv.description = VisitDescriptionTextBox.Text;
            tempv.endDate = System.DateTime.Now;
            tempv.duration = tempv.startDate - tempv.startDate;
            if (tempv.duration <= TimeSpan.Zero)
            {
                tempv.startDate = System.DateTime.Now;
            }

            if ((!string.IsNullOrWhiteSpace(CostTextBox.Text) )&& double.TryParse(CostTextBox.Text, out cost))
            {
                tempp.amount = cost;
                tempp.pymentDate = "";
                string post = JsonConvert.SerializeObject(tempp);

                if (RestService.PutInfo("payments/update", post, tempp.id))
                {
                    
                    post = JsonConvert.SerializeObject(tempv);

                    if (RestService.PutInfo("visits/update", post, tempv.id))
                    {

                        Close();
                    }
                    else
                        MessageBox.Show("wystąpił błąd podczas edytowania wizyty");

                }
                else
                    MessageBox.Show("wystąpił błąd podczas edytowania opłat");



            }
            else MessageBox.Show("usupełnij poprawnie pole koszt");


        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
