﻿using System;

namespace WET_NAM_APP.AdminWindow
{
    public class client
    {
        public string email { get; set; }
        public string password { get; set; }
        public string confirmPassword {get; set;}
        public string phoneNumber { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        
        public string sex { get; set; }
        public string usertype { get; set; }
        public string lastPasswordChange { get; set; }
        public string id { get; set; }
    }
}