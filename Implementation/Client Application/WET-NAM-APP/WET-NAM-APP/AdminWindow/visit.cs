﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WET_NAM_APP.AdminWindow
{
    public class visit
    {
        
        public string id { get; set; }

       
        public DateTime startDate { get; set; }

       
        public DateTime endDate { get; set; }

        
        public TimeSpan? duration { get; set; }

        public string payment { get; set; }

        public string pet { get; set; }

        public string user { get; set; }

        public string vet { get; set; }

       
        public string description { get; set; }

    }
}
