﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;
using WET_NAM_APP.ClientWindow;


namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy PasswordChangeWindow.xaml
    /// </summary>
    public partial class PasswordChangeWindow : Window
    {

        class changepass
        {
            public string oldPassword { get; set; }
            public string newPassword { get; set; }
            public string confirmPassword { get; set; }

        }
        public PasswordChangeWindow()
        {
            InitializeComponent();
        }

        private void ChangeButton_Click(object sender, RoutedEventArgs e)
        {

            if(!( string.IsNullOrWhiteSpace(OldPassword.Password) || string.IsNullOrWhiteSpace(NewPassword.Password) || string.IsNullOrWhiteSpace(NewPassword2.Password)))
            {
                changepass pass = new changepass() { oldPassword = OldPassword.Password, newPassword = NewPassword.Password, confirmPassword = NewPassword2.Password };
                string data = JsonConvert.SerializeObject(pass);

                if (PassValid(NewPassword.Password, NewPassword2.Password))
                {
                    if((RestService.PostInfo3("users/changePassword", data , RestService.Instance.UserID)))
                    {
                        MessageBox.Show("Hasło zostało zmienione");
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Hasło nie zostało zmienione \n sprawdź poprawność starego hasła");
                    }
                }
                
            }
            else
            {
                MessageBox.Show("wypełnij wszystkie pola");
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private bool PassValid(string pass, string pass2)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var MIN_PASS = 8;
            var MAX_PASS = 15;
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(pass) || !hasUpperChar.IsMatch(pass) || !hasNumber.IsMatch(pass) ||
                !hasSymbols.IsMatch(pass) || !(pass.Length >= MIN_PASS && pass.Length <= MAX_PASS))
            {
                MessageBox.Show(
                    "Hasło nie spełnia wymogów bezpieczeństwa!\nWymagane: długość 8-15 znaków, zawiera co najmniej jedną cyfrę, literę małą i dużą, oraz znak specjalny.",
                    "Błąd - Nie można zarejestrować uźytkownika");
                return false;
            }
            else
            {
                if (string.Compare(pass, pass2) != 0)
                {
                    MessageBox.Show("podane hasła nie są identyczne");
                    return false;
                }
                else return true;
            }
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            if (this.Owner.Title=="Okno główne - panel użytkownika")
            {
                ((UserMainWindow) this.Owner).ResetTimer();
            }
            base.OnClosing(e);
        }

    }
}
