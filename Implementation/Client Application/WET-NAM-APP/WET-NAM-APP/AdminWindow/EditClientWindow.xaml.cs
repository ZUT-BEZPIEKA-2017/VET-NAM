﻿using System;
using System.Windows;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;

namespace WET_NAM_APP.AdminWindow
{
    /// <summary>
    /// Logika interakcji dla klasy EditClientWindow.xaml
    /// </summary>
    public partial class EditClientWindow : Window
    {
        public client tempclient = new client();
        //private client editclient;
        public bool addclick;

        public EditClientWindow(client clienttoedit)
        {
            InitializeComponent();
            Title = "edytuj klienta";
            IdTextBox.Text = clienttoedit.id;
            EmailTextBox.Text = clienttoedit.email;
            NameTextBox.Text = clienttoedit.name;
            LastnameTextBox.Text = clienttoedit.surname;
            if (clienttoedit.sex == "Male")
                SexComboBox.SelectedIndex = 0;
            else SexComboBox.SelectedIndex = 1;
            PhoneNumberTextBox.Text = clienttoedit.phoneNumber;
           // editclient = clienttoedit;
            tempclient = clienttoedit;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {

            if (!(string.IsNullOrWhiteSpace(EmailTextBox.Text) || string.IsNullOrWhiteSpace(Pass1TextBox.Text) || string.IsNullOrWhiteSpace(Pass2TextBox.Text) || string.IsNullOrWhiteSpace(NameTextBox.Text) ||
                  string.IsNullOrEmpty(LastnameTextBox.Text) || string.IsNullOrWhiteSpace(PhoneNumberTextBox.Text) || string.IsNullOrWhiteSpace(SexComboBox.SelectedValue.ToString())))
            {

                if (EmailValid(EmailTextBox.Text))
                {

                    if ((string.Compare(SexComboBox.SelectedValue.ToString(), "Male") == 0) || (string.Compare(SexComboBox.SelectedValue.ToString(), "Female") == 0))
                    {
                        if (PhoneNumberValid(PhoneNumberTextBox.Text))
                        {
                            tempclient.email = EmailTextBox.Text;
                            if ((!(string.IsNullOrWhiteSpace(Pass1TextBox.Text)) || (!(string.IsNullOrWhiteSpace(Pass2TextBox.Text)))))
                            {
                                if (PassValid(Pass1TextBox.Text, Pass2TextBox.Text))
                                {
                                    tempclient.password = Pass1TextBox.Text;
                                    tempclient.confirmPassword = Pass2TextBox.Text;
                                }
                            }

                            tempclient.name = NameTextBox.Text;
                            tempclient.surname = LastnameTextBox.Text;
                            tempclient.sex = SexComboBox.SelectedValue.ToString();
                            tempclient.phoneNumber = PhoneNumberTextBox.Text;
                            tempclient.usertype = "Client";
                           // tempclient.id = editclient.id;

                            string postclient = JsonConvert.SerializeObject(tempclient);

                            if (RestService.PostInfo2("users/register", postclient))
                            {
                                addclick = true;
                                Close();
                            }
                            else
                                MessageBox.Show("wystąpił błąd podczas dodawania rekordu");



                        }
                    }
                    else MessageBox.Show("podano błędny płeć");
                }

                else MessageBox.Show("podano błędny email");
            }
            else
            {
                MessageBox.Show("wypełnij wszystkie pola");
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            addclick = false;
            Close();
        }




        private bool EmailValid(string mail)
        {
            var regex = new Regex(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" +
                          @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$");
            var match = regex.Match(mail);
            if (match.Success) return true;
            return false;
        }

        private bool PassValid(string pass, string pass2)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var MIN_PASS = 8;
            var MAX_PASS = 15;
            var hasLowerChar = new Regex(@"[a-z]+");
            var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");

            if (!hasLowerChar.IsMatch(pass) || !hasUpperChar.IsMatch(pass) || !hasNumber.IsMatch(pass) ||
                !hasSymbols.IsMatch(pass) || !(pass.Length >= MIN_PASS && pass.Length <= MAX_PASS))
            {
                MessageBox.Show(
                    "Hasło nie spełnia wymogów bezpieczeństwa!\nWymagane: długość 8-15 znaków, zawiera co najmniej jedną cyfrę, literę małą i dużą, oraz znak specjalny.",
                    "Błąd - Nie można zarejestrować uźytkownika");
                return false;
            }
            else
            {
                if (string.Compare(pass, pass2) != 0)
                {
                    MessageBox.Show("podane hasła nie są identyczne");
                    return false;
                }
                else return true;
            }
        }

        private bool PhoneNumberValid(string number)
        {

            var valueAsString = number;
            // remove '+' character and all white spaces
            valueAsString = valueAsString.Replace("+", string.Empty).Replace(" ", string.Empty);

            foreach (var c in valueAsString)
            {
                if (!char.IsDigit(c))
                {
                    MessageBox.Show(this, "Numer telefonu zawiera niedozwolone znaki!",
                        "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                    return false;
                }
            }

            if (valueAsString.Length != 9 && valueAsString.Length != 11)
            {
                MessageBox.Show(this, "Numer telefonu nie jest odpowiedniej długości!",
                    "Błąd - Nie można zarejestrować uźytkownika", MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }
            return true;

        }
    }
}