﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using WET_NAM_APP.APIclass;
using WET_NAM_APP.ClientWindow.GridItems;
using WET_NAM_APP.ClientWindow.models;

namespace WET_NAM_APP.ClientWindow
{
    /// <summary>
    /// Logika interakcji dla klasy CheckVetWindow.xaml
    /// </summary>
    public partial class CheckVetWindow : Window
    {
        private List<VetItems> vetsList;
        private List<VisitItem> visitsList;
        private DateTime selectedDate;
        public CheckVetWindow()
        {
            InitializeComponent();
            InitSelectVetComboBox();

            calendarCalendar.DisplayDateStart = DateTime.Now;
            selectedDate = DateTime.Now;
        }

        public void InitSelectVetComboBox()
        {
            if (RestService.Instance.IsToken)
            {
                string response = RestService.GetInfo("/users/vets");

                vetsList = JsonConvert.DeserializeObject<List<VetItems>>(response, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                selectVetComboBox.Items.Clear();
                foreach (VetItems vet in vetsList)
                {
                    selectVetComboBox.Items.Add(vet.name + " " + vet.surname);
                }

                response = RestService.GetInfo("/visits");

                visitsList = JsonConvert.DeserializeObject<List<VisitItem>>(response, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
            }
            else
            {
                MessageBox.Show(this, "Twoja token wygasł. Zostałeś automatycznie wylogowany z aplikacji.",
                    "Sesja wygasła", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Application.Current.Shutdown();
            }
        }

        private void selectVetComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string currentSelectedVet = vetsList[selectVetComboBox.SelectedIndex].id;

            List<VisitGrid> visits = new List<VisitGrid>();
            int index = 1;
           
            foreach (VisitItem visit in visitsList)
            {
                if (visit.vet.Equals(currentSelectedVet))
                {
                    DateTime dateTimeOfVisit = DateTime.Parse(visit.startDate);
                    if (selectedDate.Year == dateTimeOfVisit.Year &&
                        selectedDate.DayOfYear == dateTimeOfVisit.DayOfYear)
                    {
                        visits.Add(new VisitGrid() {
                            Lp = index,
                            Od = new TimeSpan(dateTimeOfVisit.Hour, dateTimeOfVisit.Minute, dateTimeOfVisit.Second).ToString(),
                            Do = new TimeSpan(dateTimeOfVisit.Hour + 1, dateTimeOfVisit.Minute, dateTimeOfVisit.Second).ToString()
                        });
                        index++;
                    }
                }
            }
            dayEventsListView.ItemsSource = visits;
        }
 
        private void calendarCalendar_SelectedDatesChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (selectVetComboBox.SelectedIndex == -1)
            {
                return;
            }
            string currentSelectedVet = vetsList[selectVetComboBox.SelectedIndex].id;

            List<VisitGrid> visits = new List<VisitGrid>();
            int index = 1;
            selectedDate = calendarCalendar.SelectedDate.Value;

            foreach (VisitItem visit in visitsList)
            {
                if (visit.vet.Equals(currentSelectedVet))
                {
                    DateTime dateTimeOfVisit = DateTime.Parse(visit.startDate);
                    if (selectedDate.Year == dateTimeOfVisit.Year &&
                        selectedDate.DayOfYear == dateTimeOfVisit.DayOfYear)
                    {
                        visits.Add(new VisitGrid()
                        {
                            Lp = index,
                            Od = new TimeSpan(dateTimeOfVisit.Hour, dateTimeOfVisit.Minute, dateTimeOfVisit.Second).ToString(),
                            Do = new TimeSpan(dateTimeOfVisit.Hour + 1, dateTimeOfVisit.Minute, dateTimeOfVisit.Second).ToString()
                        });
                        index++;
                    }
                }
            }
            dayEventsListView.ItemsSource = visits;
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            ((UserMainWindow)this.Owner).ResetTimer();
            base.OnClosing(e);
        }
        
    }
}