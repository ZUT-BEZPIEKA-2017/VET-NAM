﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using WET_NAM_APP.ClientWindow.GridItems;

namespace WET_NAM_APP.ClientWindow
{
    /// <summary>
    /// Logika interakcji dla klasy MyPetWindow.xaml
    /// </summary>
    public partial class MyPetWindow : Window
    {
        public MyPetWindow()
        {
            InitializeComponent();
            DownloadMyPets();
        }

        private void myPetListView_Initialized(object sender, EventArgs e)
        {
            var view = new GridView();

            var col1 = new GridViewColumn
            {
                Header = "Unikalny identyfikator",
                DisplayMemberBinding = new Binding("Id")
            };
            var col2 = new GridViewColumn
            {
                Header = "Imię",
                DisplayMemberBinding = new Binding("Name")
            };
            var col3 = new GridViewColumn
            {
                Header = "Rasa",
                DisplayMemberBinding = new Binding("Breed")
            };
            var col4 = new GridViewColumn
            {
                Header = "Płeć",
                DisplayMemberBinding = new Binding("Sex")
            };
            var col5 = new GridViewColumn
            {
                Header = "Wiek",
                DisplayMemberBinding = new Binding("Age")
            };
            var col6 = new GridViewColumn
            {
                Header = "Opis",
                DisplayMemberBinding = new Binding("Description")
            };
            view.Columns.Add(col1);
            view.Columns.Add(col2);
            view.Columns.Add(col3);
            view.Columns.Add(col4);
            //  view.Columns.Add(col5);
            view.Columns.Add(col6);

            myPetListView.View = view;
        }

        private void DownloadMyPets()
        {
            var request = "/pets/user/" + RestService.Instance.UserID;
            var response = RestService.GetInfo(request);
            if (response == "[]")
            {
                MessageBox.Show(this,
                    "Wyglda na to, że nie masz jeszcze dodanego zwierzaka. Możesz dodać go właśnie teraz!",
                    "Brak zwierzaczka", MessageBoxButton.OK, MessageBoxImage.Information);
                TurnOffButtons();
                myPetListView.Items.Clear();
            }
            else
            {
                AddPetsToTable(response);
                TurnOnButtons();
            }
        }


        private void AddPetsToTable(string response)
        {
            myPetListView.Items.Clear();
            dynamic JSON = JsonConvert.DeserializeObject(response);
            var jsonObj = (JArray) JsonConvert.DeserializeObject(response);
            int count = jsonObj.Count;
            for (int i = 0; i < count; i++)
            {
                myPetListView.Items.Add(new PetsItem
                {
                    Id = JSON[i].id,
                    Name = JSON[i].name,
                    Breed = JSON[i].breed,
                    Description = JSON[i].description,
                    Owner = JSON[i].owner,
                    Sex = JSON[i].sex
                });
            }
            myPetListView.SelectedIndex = 0;
        }

        private void TurnOffButtons()
        {
            deletePetButton.IsEnabled = false;
            editPetButton.IsEnabled = false;
        }

        private void TurnOnButtons()
        {
            deletePetButton.IsEnabled = true;
            editPetButton.IsEnabled = true;
        }

        private void deletePetButton_Click(object sender, RoutedEventArgs e)
        {
            if (myPetListView.HasItems && myPetListView.SelectedItem != null)
            {
                var petObject = myPetListView.SelectedItems[0] as PetsItem;
                if (petObject == null)
                {
                    return;
                }
                if (
                    MessageBox.Show(this, "Czy na pewno chcesz usunąć " + petObject.Breed + " " + petObject.Name + "?",
                        "Usuwanie zwierzaka", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    if (RestService.DelInfo("pets/delete", petObject.Id))
                    {
                        MessageBox.Show(this, "Operacja zakończona powodzeniem!");
                    }
                    else
                    {
                        MessageBox.Show(this, "Wystąpił błąd.");
                    }
                }
                DownloadMyPets();
            }
        }

        private void editPetButton_Click(object sender, RoutedEventArgs e)
        {
            if (myPetListView.HasItems && myPetListView.SelectedItem != null)
            {
                var petObject = myPetListView.SelectedItems[0] as PetsItem;
                if (petObject == null)
                {
                    return;
                }
                Window editPet = new myPet.EditPetWindow(petObject.Id, petObject.Name, petObject.Breed,
                    petObject.Description, petObject.Sex);
                editPet.ShowDialog();
                DownloadMyPets();
            }
        }

        private void addPetButton_Click(object sender, RoutedEventArgs e)
        {
            Window addPet = new myPet.AddPetWindow();
            addPet.ShowDialog();
            DownloadMyPets();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            ((UserMainWindow)this.Owner).ResetTimer();
            base.OnClosing(e);
        }
    }
}