﻿using System.Windows;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;

namespace WET_NAM_APP.ClientWindow.myPet
{
    /// <summary>
    /// Logika interakcji dla klasy EditPetWindow.xaml
    /// </summary>
    public partial class EditPetWindow : Window
    {
        private string _petId;

        public EditPetWindow(string id, string name, string breed, string description, string sex)
        {
            InitializeComponent();
            _petId = id;
            nameTextBox.Text = name;
            breedTextBox.Text = breed;
            descriptionTextBox.Text = description;
            if (sex == "Male")
            {
                ManRadioButton.IsChecked = true;
            }
            else
            {
                WomenRadioButton.IsChecked = true;
            }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                var json = PrepareData();
                if (RestService.PutInfo("pets/update", json, _petId))
                {
                    if (
                        MessageBox.Show("Zwierzak został zedytowany", "", MessageBoxButton.OK,
                            MessageBoxImage.Information) == MessageBoxResult.OK)
                    {
                        Close();
                    }
                }
                else
                {
                    MessageBox.Show("Błąd", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private string PrepareData()
        {
            string sex;
            if (WomenRadioButton.IsChecked != null && (bool) WomenRadioButton.IsChecked)
            {
                sex = "Female";
            }
            else
            {
                sex = "Male";
            }

            var o = new JObject
            {
                {"name", nameTextBox.Text},
                {"owner", RestService.Instance.UserID},
                {"breed", breedTextBox.Text},
                {"description", descriptionTextBox.Text},
                {"sex", sex}
            };
            return o.ToString();
        }

        private bool ValidateData()
        {
            if (WomenRadioButton.IsChecked != null &&
                (ManRadioButton.IsChecked != null &&
                 (string.IsNullOrWhiteSpace(nameTextBox.Text) || string.IsNullOrWhiteSpace(breedTextBox.Text) ||
                  (!(bool) WomenRadioButton.IsChecked && !(bool) ManRadioButton.IsChecked))))
            {
                MessageBox.Show(this, "Należy wypełnić wszystkie pola!", "Błąd - Nie można dodać",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}