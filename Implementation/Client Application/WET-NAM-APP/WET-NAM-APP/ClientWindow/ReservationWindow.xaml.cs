﻿using System.Windows;
using WET_NAM_APP.APIclass;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.AdminWindow;
using System.Collections.Generic;
using WET_NAM_APP.ClientWindow.GridItems;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;

namespace WET_NAM_APP.ClientWindow
{
    /// <summary>
    /// Logika interakcji dla klasy ReservationWindow.xaml
    /// </summary>
    public partial class ReservationWindow : Window
    {
        private List<PetsItem> petsList;
        private List<VetItems> vetsList;
        private List<VisitsSimpleItem> visitsList;
        public ReservationWindow()
        {
            InitializeComponent();

            reserveDate.DisplayDateStart = DateTime.Now;
            
            InitPetNameReserve();
            InitPrefferedVetComboBox();
        }

        public void InitPrefferedVetComboBox()
        {
            if (RestService.Instance.IsToken)
            {
                string response = RestService.GetInfo("/users/vets");

                vetsList = JsonConvert.DeserializeObject<List<VetItems>>(response ,new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                preferredVetComboBox.Items.Clear();
                foreach (VetItems vet in vetsList)
                {
                    preferredVetComboBox.Items.Add(vet.name + " " + vet.surname);
                }
            }
            else
            {
                MessageBox.Show(this, "Twoja token wygasł. Zostałeś automatycznie wylogowany z aplikacji.",
                    "Sesja wygasła", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Application.Current.Shutdown();
            }
        }

        public void InitPetNameReserve()
        {
            if (RestService.Instance.IsToken)
            {
                string response = RestService.GetInfo("/pets/user/" + RestService.Instance.UserID);
                petsList = JsonConvert.DeserializeObject<List<PetsItem>>(response, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                petNameReserve.Items.Clear();
                foreach (PetsItem pet in petsList)
                {
                    petNameReserve.Items.Add(pet.Name);
                }
            }
            else
            {
                MessageBox.Show(this, "Twoja token wygasł. Zostałeś automatycznie wylogowany z aplikacji.",
                                    "Sesja wygasła", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Application.Current.Shutdown();
            }
        }

        private void preferredVetComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            reserveDate.IsEnabled = true;
        }

        private void reserveDate_SelectedDateChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (RestService.Instance.IsToken)
            {
                string response = RestService.GetInfo("/visits");

                visitsList = JsonConvert.DeserializeObject<List<VisitsSimpleItem>>(response, new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                DateTime selectedDate = reserveDate.SelectedDate.Value;
                string selectedVet = vetsList[preferredVetComboBox.SelectedIndex].id;
                List<int> availbleHours = new List<int> { 8, 9, 10, 11, 12, 13, 14, 15 };
                
                foreach (VisitsSimpleItem visit in visitsList)
                {
                    if (visit.vet.Equals(selectedVet))
                    {
                        DateTime dateTimeOfVisit = DateTime.Parse(visit.startDate);
                        if (dateTimeOfVisit.Year == selectedDate.Year &&
                            dateTimeOfVisit.DayOfYear == selectedDate.DayOfYear)
                        {
                            availbleHours.Remove(dateTimeOfVisit.Hour);
                        }
                    }
                }

                timeComboBox.Items.Clear();
                foreach (int i in availbleHours)
                {
                    timeComboBox.Items.Add(i + ":00 - " + (i + 1) + ":00");
                }
                timeComboBox.IsEnabled = true;
            }
            else
            {
                MessageBox.Show(this, "Twoja token wygasł. Zostałeś automatycznie wylogowany z aplikacji.",
                                    "Sesja wygasła", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Application.Current.Shutdown();
            }
        }

        private void reserveButton_Click(object sender, RoutedEventArgs e)
        {
            if (preferredVetComboBox.SelectedIndex == -1 ||
                petNameReserve.SelectedIndex == -1 ||
                timeComboBox.SelectedIndex == -1)
            {
                MessageBox.Show("Wypełnij wszystkie pola!", "Błąd");
                return;
            }
            else
            {
                PaymentItem payment = new PaymentItem();
                payment.id = Guid.NewGuid().ToString();
                payment.amount = 0;
                payment.paymentDay = null;
                payment.status = "InProgress";
                payment.user = RestService.Instance.UserID;

                string data = JsonConvert.SerializeObject(payment);

                bool resultBool = RestService.PostInfo2("/payments/add", data);

                if (!resultBool)
                {
                    MessageBox.Show("Wystąpił błąd aplikacji - payment", "Błąd");
                    return;
                }

                Char splitter = ':';
                string[] splittedString = timeComboBox.SelectedValue.ToString().Split(splitter);
                double hoursToAdd = Double.Parse(splittedString[0], NumberStyles.Number);
                DateTime startDate = reserveDate.SelectedDate.Value.AddHours(hoursToAdd);
                TimeSpan duration = new TimeSpan(1, 0, 0);
                DateTime endDate = startDate.Add(duration);

                VisitItem visitItem = new VisitItem();

                visitItem.startDate = startDate.ToString("o");
                visitItem.endDate = endDate.ToString("o");
                visitItem.duration = duration.ToString();
                visitItem.payment = payment.id;
                visitItem.pet = petsList[petNameReserve.SelectedIndex].Id;
                visitItem.user = RestService.Instance.UserID;
                visitItem.vet = vetsList[preferredVetComboBox.SelectedIndex].id;
                visitItem.description = descriptionTextBox.Text;

                data = JsonConvert.SerializeObject(visitItem);
                resultBool = RestService.PostInfo2("/visits/add", data);

                if (!resultBool)
                {
                    MessageBox.Show("Wystąpił błąd aplikacji - reservation", "Błąd");
                    return;
                }
                else
                {
                    MessageBox.Show("Pomyslnie dodan wizyte", "Sukces");
                    this.Close();
                }
            }
        }

        private void cancelReserveButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            ((UserMainWindow)this.Owner).ResetTimer();
            base.OnClosing(e);
        }
    }
}