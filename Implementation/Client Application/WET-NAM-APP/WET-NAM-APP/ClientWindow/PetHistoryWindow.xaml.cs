﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WET_NAM_APP.APIclass;
using WET_NAM_APP.ClientWindow.GridItems;

namespace WET_NAM_APP.ClientWindow
{
    /// <summary>
    /// Logika interakcji dla klasy PetHistoryWindow.xaml
    /// </summary>
    public partial class PetHistoryWindow : Window
    {
        private List<PetsItem> _petsList;

        public PetHistoryWindow()
        {
            InitializeComponent();
            DownloadMyPets();
        }

        private void DownloadMyPets()
        {
            if (RestService.Instance.IsToken)
            {
                var response = RestService.GetInfo("/pets/user/" + RestService.Instance.UserID);

                _petsList = JsonConvert.DeserializeObject<List<PetsItem>>(response, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                selectPetComboBox.Items.Clear();
                foreach (PetsItem pet in _petsList)
                {
                    selectPetComboBox.Items.Add(pet.Name);
                }
            }
            else
            {
                MessageBox.Show(this, "Twoja token wygasł. Zostałeś automatycznie wylogowany z aplikacji.",
                    "Sesja wygasła", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Application.Current.Shutdown();
            }
        }

        private void petHistoryListView_Initialized(object sender, EventArgs e)
        {
            var view = new GridView();

            var col1 = new GridViewColumn
            {
                Header = "Lp",
                DisplayMemberBinding = new Binding("Position")
            };
            var col2 = new GridViewColumn
            {
                Header = "Data rozpoczęcia",
                DisplayMemberBinding = new Binding("Date")
            };
            var col3 = new GridViewColumn
            {
                Header = "Godzina zakończenia",
                DisplayMemberBinding = new Binding("Time")
            };
            var col4 = new GridViewColumn
            {
                Header = "Weterynarz",
                DisplayMemberBinding = new Binding("Vet")
            };
            var col5 = new GridViewColumn
            {
                Header = "Status płatności",
                DisplayMemberBinding = new Binding("Payment")
            };
            var col6 = new GridViewColumn
            {
                Header = "Kwota",
                DisplayMemberBinding = new Binding("Amount")
            };
            var col7 = new GridViewColumn
            {
                Header = "Opis zdarzenia",
                DisplayMemberBinding = new Binding("Event")
            };
            view.Columns.Add(col1);
            view.Columns.Add(col2);
            view.Columns.Add(col3);
            view.Columns.Add(col4);
            view.Columns.Add(col5);
            view.Columns.Add(col6);
            view.Columns.Add(col7);

            petHistoryListView.View = view;
        }

        private void selectPetComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadInfoAboutPet(_petsList[selectPetComboBox.SelectedIndex].Id);
        }

        private void LoadInfoAboutPet(string id)
        {
            var request = "/visits/pet/" + id;
            var response = RestService.GetInfo(request);

            petHistoryListView.Items.Clear();
            if (response == "")
            {
                MessageBox.Show(this,
                    "Wyglda na to, że ten zwierzak nie był jeszcze na żanej wizycie.",
                    "Brak wizyt do wyświetlenia", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                dynamic JSON = JsonConvert.DeserializeObject(response);
                var jsonObj = (JArray) JsonConvert.DeserializeObject(response);
                int count = jsonObj.Count;
                for (int i = 0; i < count; i++)
                {
                    petHistoryListView.Items.Add(new PetHistory
                    {
                        Position = i.ToString(),
                        Date = JSON[i].startDate,
                        Time = JSON[i].endDate,
                        Vet = GetVetName(JSON[i].vet),
                        Payment = GetPaymentStatus(JSON[i].payment),
                        Amount = GetPaymentAmount(JSON[i].payment),
                        Event = JSON[i].description
                    });
                }
            }
        }

        private string GetPaymentAmount(object paymentId)
        {
            var request = "/payments/" + paymentId;
            var response = RestService.GetInfo(request);

            dynamic JSON = JsonConvert.DeserializeObject(response);
            return JSON[0].amount;
        }

        private string GetPaymentStatus(object paymentId)
        {
            var request = "/payments/" + paymentId;
            var response = RestService.GetInfo(request);

            dynamic JSON = JsonConvert.DeserializeObject(response);
            return JSON[0].status;
        }

        private string GetVetName(object vetId)
        {
            var request = "/users/" + vetId;
            var response = RestService.GetInfo(request);

            if (response != "[]")
            {
                dynamic JSON = JsonConvert.DeserializeObject(response);
                return JSON[0].name + " " + JSON[0].surname;
            }
            else
            {
                return "Brak lekarza w bazie danych";
            }            
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            ((UserMainWindow)this.Owner).ResetTimer();
            base.OnClosing(e);
        }
    }
}