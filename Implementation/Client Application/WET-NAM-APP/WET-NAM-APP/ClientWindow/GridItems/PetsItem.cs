﻿namespace WET_NAM_APP.ClientWindow.GridItems
{
    class PetsItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Breed { get; set; }
        public string Description { get; set; }
        public string Sex { get; set; }
    }
}