﻿namespace WET_NAM_APP.ClientWindow.GridItems
{
    class PetHistory
    {
        public string Position { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Vet { get; set; }
        public string Payment { get; set; }
        public string Amount { get; set; }
        public string Event { get; set; }
    }
}