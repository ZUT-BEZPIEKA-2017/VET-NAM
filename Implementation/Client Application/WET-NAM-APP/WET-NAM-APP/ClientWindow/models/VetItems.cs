﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WET_NAM_APP.ClientWindow.GridItems
{
    /**
     * Simple class representing main attributes of vet
     * TODO
     */
    class VetItems
    {
        public string id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
    }
}
