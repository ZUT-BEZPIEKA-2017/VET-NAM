﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WET_NAM_APP.ClientWindow.GridItems
{
    class VisitItem
    {
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string duration { get; set; }
        public string payment { get; set; }
        public string pet { get; set; }
        public string user { get; set; }
        public string vet { get; set; }
        public string description { get; set; }
    }
}
