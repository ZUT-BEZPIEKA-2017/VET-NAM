﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WET_NAM_APP.ClientWindow.models
{
    class VisitGrid
    {
        public int Lp { get; set; }
        public string Od { get; set; }
        public string Do { get; set; }
    }
}
