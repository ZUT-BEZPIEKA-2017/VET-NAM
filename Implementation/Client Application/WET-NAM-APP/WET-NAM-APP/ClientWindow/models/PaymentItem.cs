﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WET_NAM_APP.ClientWindow
{
    /**
     * Simple class representing main attributes of payment
     * TODO
     */
    class PaymentItem
    {
        public string id { get; set; }
        public int amount { get; set; }
        public string paymentDay { get; set; }
        public string status { get; set; }
        public string user { get; set; }
    }
}
