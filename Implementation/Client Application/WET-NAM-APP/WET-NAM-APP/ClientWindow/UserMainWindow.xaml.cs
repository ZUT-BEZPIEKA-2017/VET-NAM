﻿using System;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Newtonsoft.Json;
using RestSharp.Extensions;
using WET_NAM_APP.AdminWindow;
using WET_NAM_APP.APIclass;

namespace WET_NAM_APP.ClientWindow
{
    /// <summary>
    /// Logika interakcji dla klasy UserMainWindow.xaml
    /// </summary>
    public partial class UserMainWindow : Window
    {
        private DispatcherTimer _timer;
        private TimeSpan _time;
        private const int SESSION_TIME = 59;

        public UserMainWindow()
        {
            InitializeComponent();
            usernameLabel.Content = RestService.Instance.Login;
            RunTimer();
            _timer.Start();
            GetLastLoginValue();
        }

        private void GetLastLoginValue()
        {
            var request = "/users/" + RestService.Instance.UserID;
            var response = RestService.GetInfo(request);
            if (response != "[]")
            {
                dynamic JSON = JsonConvert.DeserializeObject(response);
                DateTime utcTime = DateTime.SpecifyKind(DateTime.Parse(JSON[0].lastCorrectLogin.ToString()), DateTimeKind.Utc);
                lastLoginValueLabel.Content = utcTime.ToLocalTime().ToString(@"R");
            }
        }

        private void RunTimer()
        {
            _time = TimeSpan.FromSeconds(SESSION_TIME);
            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                sessionExpireValueLabel.Content = _time.ToString("c");
                if (_time == TimeSpan.Zero)
                {
                    _timer.Stop();
                    MessageBox.Show(this, "Twoja sesja wygasła. Zostałeś automatycznie wylogowany z aplikacji.",
                        "Sesja wygasła", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Application.Current.Shutdown();
                }
                _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);
        }

        public void ResetTimer()
        {
            RestService.GetToken(); 
            _time = TimeSpan.FromSeconds(SESSION_TIME);
        }

        private void reserveButtonButton_Click(object sender, RoutedEventArgs e)
        {
            ResetTimer();
            Window window = new ReservationWindow();
            window.Owner = this;
            window.Show();
        }

        private void manageMyPetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetTimer();
            Window window = new MyPetWindow();
            window.Owner = this;
            window.Show();
        }

        private void checkVetButton_Click(object sender, RoutedEventArgs e)
        {
            ResetTimer();
            Window window = new CheckVetWindow();
            window.Owner = this;
            window.Show();
        }

        private void petHistoryButton_Click(object sender, RoutedEventArgs e)
        {
            ResetTimer();
            Window window = new PetHistoryWindow();
            window.Owner = this;
            window.Show();
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
           if(MessageBox.Show(this, "Czy na pewno chcesz zamknąć sesję i wylogować się z aplikacji?",
                "Potwierdzenie wylogowania", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
            
        }

        private void changePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            ResetTimer();
            PasswordChangeWindow window = new PasswordChangeWindow()
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            window.ShowDialog();

        }
    }
}