﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LogParser
{
    public class Parser
    {
        private readonly string[] _logContentStrings;
        private readonly List<Log> _dataLogModels = new List<Log>();
        private const string StartPattern = @"(\d\d\d\d-\d\d-\d\d)(\ )(\d\d:\d\d:\d\d)(\ )"; //Januszek :(
        
        public Parser(string filePath)
        {
            var lines = System.IO.File.ReadAllLines(filePath);
            _logContentStrings = lines;
        }

        public void Parse()
        {
            if (_logContentStrings == null || _logContentStrings.Length < 1) return;

            var r = new Regex(StartPattern);
            foreach (var line in _logContentStrings)
            {
                var m = r.Match(line);
                if (!m.Success) continue;
                ParseLine(line);
            }
        }

        private void ParseLine(string line)
        {
            var entryLogModel = new Log();
            var parts = line.Split(' ');

            entryLogModel.Date = parts.ElementAtOrDefault(0);
            entryLogModel.Time = parts.ElementAtOrDefault(1);
            entryLogModel.SIp = parts.ElementAtOrDefault(2);
            entryLogModel.CsMethod = parts.ElementAtOrDefault(3);
            entryLogModel.CsUriStem = parts.ElementAtOrDefault(4);
            entryLogModel.CsUriQuery = parts.ElementAtOrDefault(5);
            entryLogModel.SPort = parts.ElementAtOrDefault(6);
            entryLogModel.CsUserName = parts.ElementAtOrDefault(7);
            entryLogModel.CIp = parts.ElementAtOrDefault(8);
            entryLogModel.CsUserAgent = parts.ElementAtOrDefault(9);
            entryLogModel.CsRefferer = parts.ElementAtOrDefault(10);
            entryLogModel.ScStatus = parts.ElementAtOrDefault(11);
            entryLogModel.ScSubStatus = parts.ElementAtOrDefault(12);
            entryLogModel.ScWin32Status = parts.ElementAtOrDefault(13);
            entryLogModel.TimeTaken = parts.ElementAtOrDefault(14);

            _dataLogModels.Add(entryLogModel);
        }

        public List<Log> GetDataModel()
        {
            return _dataLogModels;
        }
    }
}