﻿using System.Collections.Generic;
using System.Windows;
using LogParser;

namespace AdminParser
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Log> _parsedData;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadFileButton_Click(object sender, RoutedEventArgs e)
        {
            string filename = OpenFileDialog();
            if (filename == null) return;

            Parser parser = new Parser(filename);
            parser.Parse();
            _parsedData = parser.GetDataModel();
            DataGrid.ItemsSource = _parsedData;
        }

        private string OpenFileDialog()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                DefaultExt = ".log",
                Filter = "LOG files (*.log)|*.log"
            };
            bool? result = dlg.ShowDialog();
            if (result == true)
            {
                return dlg.FileName;
            }
            return null;
        }
    }
}